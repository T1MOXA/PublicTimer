public void race_MainMenu(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(RaceMainMenuHandle);

	Format(C_buffer, 512, "%s - Races", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Race a Player");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Show Statistics");
	M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);

	M_Menu.Display(I_Client, 0);
}

public int RaceMainMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				race_PreStart(I_Client);
			} case 1: {

			}
		}
	}
}

public void race_PreStart(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(PreStartHandle);

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int PreStartHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {

			} case 1: {

			}
		}
	}
}
