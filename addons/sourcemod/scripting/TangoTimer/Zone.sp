public void zone_ZoneCache(int I_ZoneId, int I_ZoneType, int I_ZoneGroup, float F_PointA[3], float F_PointB[3]) {
	int I_TotalZones;
	float F_MidPoint[3];

	I_TotalZones = gA_Zones.Length;

	gA_Zones.Resize(I_TotalZones + 1);

	gA_Zones.Set(I_TotalZones, I_ZoneId, 0);
	gA_Zones.Set(I_TotalZones, I_ZoneType, 1);
	gA_Zones.Set(I_TotalZones, I_ZoneGroup, 2);

	for (int i = 0; i < 3; i++) {
		gA_Zones.Set(I_TotalZones, F_PointA[i], i + 3);
		gA_Zones.Set(I_TotalZones, F_PointB[i], i + 6);
	}

	if (I_ZoneType == 2 || I_ZoneType == 5) {
		gI_CheckpointTotalCount[I_ZoneGroup]++;
		gA_Zones.Set(I_TotalZones, gI_CheckpointTotalCount[I_ZoneGroup], 9);
	} else {
		gA_Zones.Set(I_TotalZones, 0, 9);
	}


	for (int i = 0; i < 2; i++) {
		F_MidPoint[i] = ((F_PointA[i] + F_PointB[i]) / 2);
	}

	if (F_PointA[2] < F_PointB[2]) {
		F_MidPoint[2] = F_PointA[2];
	} else {
		F_MidPoint[2] = F_PointB[2];
	}

	F_MidPoint[2] += 10.0;

	if (I_ZoneType > 2) {
		I_ZoneType -= 3;
	}

	switch (I_ZoneType) {
		case 0: {
			Array_Copy(F_MidPoint, gF_ZoneSpawns[I_ZoneGroup][0], 3);
		} case 1: {
			Array_Copy(F_MidPoint, gF_ZoneSpawns[I_ZoneGroup][63], 3);
		} case 2: {
			Array_Copy(F_MidPoint, gF_ZoneSpawns[I_ZoneGroup][gI_CheckpointTotalCount[I_ZoneGroup]], 3);
		}
	}
}

public void zone_ZoneClear() {
	zone_EntityClear();

	delete gA_Zones;
	gA_Zones = new ArrayList(10);

	gI_ZoneTemp = 0;

	for (int i = 0; i < 64; i++) {
		for (int x = 0; x < 64; x++) {
			for (int y = 0; y < 3; y++) {
				gF_ZoneSpawns[i][x][y] = 0.0;
			}
		}

		for (int x = 0; x < gI_TotalStyles; x++) {
			delete gA_MapRecords[i][x];
			gA_MapRecords[i][x] = new ArrayList(7);
		}

		gI_CheckpointTotalCount[i] = 0;
	}

	gI_TotalZoneGroups = 0;
}

public void zone_ZoneEnd() {
	zone_ZoneClear();
	gB_ServerLoaded = false;

	delete gA_Names;
	gA_Names = new ArrayList(128);

	delete gA_SteamIds;
	gA_SteamIds = new ArrayList(64);

	delete gA_ReplayQueue;
	gA_ReplayQueue = new ArrayList(3);

	for (int i = 0; i < 64; i++) {
		for (int x = 0; x < gI_TotalStyles; x++) {
			for (int y = 0; y < 2; y++) {
				gI_TimerReplayRecords[i][x][y] = 0;
			}

			delete gA_ReplayFrames[i][x];
			gA_ReplayFrames[i][x] = new ArrayList(6);
		}

		for (int x = 0; x < 2; x++) {
			gI_MapInfo[i][x] = 0;
		}
	}

	if (gH_ZoneTimer != INVALID_HANDLE) {
		CloseHandle(gH_ZoneTimer);
		gH_ZoneTimer = INVALID_HANDLE;
	}

	if (gH_SecondTimer != INVALID_HANDLE) {
		CloseHandle(gH_SecondTimer);
		gH_SecondTimer = INVALID_HANDLE;
	}
}

public void zone_EntityClear() {
	char[] C_EntityName = new char[512];

	if (gA_Zones.Length > 0) {
		for (int i = 0; i <= GetMaxEntities(); i++) {
			if (IsValidEdict(i) && IsValidEntity(i)) {
				if (GetEntPropString(i, Prop_Data, "m_iName", C_EntityName, 512)) {
					if (StrContains(C_EntityName, "Timer_Zone") > -1) {
						AcceptEntityInput(i, "Kill");
					}
				}
			}
		}
	}
}

public void zone_EntityReload() {
	float F_PointA[3], F_PointB[3];

	zone_EntityClear();

	for (int i = 0; i < gA_Zones.Length; i++) {
		int I_ZoneType;

		I_ZoneType = gA_Zones.Get(i, 1);

		for (int x = 0; x < 3; x++) {
			F_PointA[x] = gA_Zones.Get(i, x + 3);
			F_PointB[x] = gA_Zones.Get(i, x + 6);
		}

		if ((I_ZoneType == 0) || (I_ZoneType == 3)) {
			if (F_PointA[2] < F_PointB[2]) {
				F_PointB[2] = F_PointA[2] + 10.0;
			} else {
				F_PointA[2] = F_PointB[2] + 10.0;
			}
		}

		zone_EntityAdd(i, F_PointA, F_PointB);
	}
}

public void zone_EntityAdd(int I_Index, float F_PointA[3], float F_PointB[3]) {
	char[] C_buffer = new char[512];
	float F_MidPoint[3], F_VecMin[3], F_VecMax[3];
	int I_Entity;

	I_Entity = CreateEntityByName("trigger_multiple");

	if (I_Entity > 0) {
		SetEntityModel(I_Entity, "models/props/de_train/barrel.mdl");

		if (IsValidEntity(I_Entity)) {
			Format(C_buffer, 512, "%i: Timer_Zone", I_Index);

			DispatchKeyValue(I_Entity, "spawnflags", "257");
			DispatchKeyValue(I_Entity, "StartDisabled", "0");
			DispatchKeyValue(I_Entity, "targetname", C_buffer);

			if (DispatchSpawn(I_Entity)) {
				ActivateEntity(I_Entity);

				for (int i = 0; i < 3; i++) {
					F_MidPoint[i] = ((F_PointA[i] + F_PointB[i]) / 2.0);
				}

				MakeVectorFromPoints(F_MidPoint, F_PointA, F_VecMin);
				MakeVectorFromPoints(F_PointB, F_MidPoint, F_VecMax);

				for (int i = 0; i < 3; i++) {
					if (F_VecMin[i] > 0.0) {
						F_VecMin[i] *= -1;
					} else if (F_VecMax[i] < 0.0) {
						F_VecMax[i] *= -1;
					}
				}

				SetEntPropVector(I_Entity, Prop_Send, "m_vecMins", F_VecMin);
				SetEntPropVector(I_Entity, Prop_Send, "m_vecMaxs", F_VecMax);

				SetEntProp(I_Entity, Prop_Send, "m_nSolidType", 2);
				SetEntProp(I_Entity, Prop_Send, "m_fEffects", GetEntProp(I_Entity, Prop_Send, "m_fEffects") | 32);

				TeleportEntity(I_Entity, F_MidPoint, NULL_VECTOR, NULL_VECTOR);
				SDKHook(I_Entity, SDKHook_StartTouch, Entity_StartTouch);
				SDKHook(I_Entity, SDKHook_EndTouch, Entity_EndTouch);
			}
		}
	}
}

public Action Entity_StartTouch(int I_Caller, int I_Activator) {
	if (IsValidClient(I_Activator)) {
		char[] C_EntityName = new char[512];
		char[] C_EntityIndex = new char[16];
		int I_Index, I_ZoneType, I_ZoneGroup, I_Checkpoint;

		GetEntPropString(I_Caller, Prop_Data, "m_iName", C_EntityName, 512);
		SplitString(C_EntityName, ":", C_EntityIndex, 16);

		I_Index = StringToInt(C_EntityIndex);
		I_ZoneType = gA_Zones.Get(I_Index, 1);
		I_ZoneGroup = gA_Zones.Get(I_Index, 2);
		I_Checkpoint = gA_Zones.Get(I_Index, 9);

		gI_TimerCurrentZone[I_Activator] = I_ZoneType;

		if (I_ZoneType == 0) {
			gB_TimerInBonus[I_Activator] = false;
		} else {
			gB_TimerInBonus[I_Activator] = true;
		}

		if (I_ZoneType > 2) {
			I_ZoneType = I_ZoneType - 3;
		}

		switch (I_ZoneType) {
			case 0: { //Player Entered Start
				if (!gB_TimerRecentlyAbused[I_Activator]) {
					timer_StopTime(I_Activator);
				}

				gI_TimerCurrentZoneGroup[I_Activator] = I_ZoneGroup;
			} case 1: { //Player Entered End
				if ((gB_TimerTimerStarted[I_Activator]) && (gI_TimerCurrentZoneGroup[I_Activator] == I_ZoneGroup) && (gI_TimerCurrentCheckpointStage[I_Activator] == gI_CheckpointTotalCount[I_ZoneGroup]) && (!gB_TimerRecentlyAbused[I_Activator]) && !gB_TimerTimerPaused[I_Activator]) {
					timer_PlayerFinished(I_Activator, I_Index);
				}

				timer_StopTime(I_Activator);
			} case 2: { //Player Entered Checkpoint
				if (gB_TimerTimerStarted[I_Activator] && (gI_TimerCurrentZoneGroup[I_Activator] == I_ZoneGroup) && (gI_TimerCurrentCheckpointStage[I_Activator] + 1 == I_Checkpoint) && (!gB_TimerRecentlyAbused[I_Activator]) && !gB_TimerTimerPaused[I_Activator]) {
					timer_PlayerCheckpoint(I_Activator, I_Index);
				}
			}
		}
	}
}

public Action Entity_EndTouch(int I_Caller, int I_Activator) {
	if (IsValidClient(I_Activator)) {
		char[] C_EntityName = new char[512];
		char[] C_EntityIndex = new char[16];
		int I_Index, I_ZoneType;

		GetEntPropString(I_Caller, Prop_Data, "m_iName", C_EntityName, 512);
		SplitString(C_EntityName, ":", C_EntityIndex, 16);

		I_Index = StringToInt(C_EntityIndex);
		I_ZoneType = gA_Zones.Get(I_Index, 1);

		if (I_ZoneType > 2) {
			I_ZoneType = I_ZoneType - 3;
		}

		switch (I_ZoneType) {
			case 0: { //Player Left Start
				if (!gB_TimerRecentlyAbused[I_Activator] && !gB_TimerTimerPaused[I_Activator] && !gB_TimerTimerNoclip[I_Activator]) {
					timer_StartTime(I_Activator, I_Index);
				}
			} case 1: { //Player Left End

			} case 2: { //Player Left Checkpoint

			}
		}

		if (!gB_TimerRecentlyAbused[I_Activator]) {
			gI_TimerCurrentZone[I_Activator] = -1;
		}
	}
}

public void zone_ZoneDraw(int I_Client, float F_PointA[3], float F_PointB[3], int I_Color, float F_Display, bool B_All) {
	float F_Points[8][3];
	int I_Colors[4];

	F_Points[0] = F_PointA;

	F_Points[1][0] = F_PointA[0];
	F_Points[1][1] = F_PointB[1];
	F_Points[1][2] = F_PointA[2];

	F_Points[2][0] = F_PointB[0];
	F_Points[2][1] = F_PointA[1];
	F_Points[2][2] = F_PointA[2];

	F_Points[3][0] = F_PointB[0];
	F_Points[3][1] = F_PointB[1];
	F_Points[3][2] = F_PointA[2];

	F_Points[4][0] = F_PointA[0];
	F_Points[4][1] = F_PointA[1];
	F_Points[4][2] = F_PointB[2];

	F_Points[5][0] = F_PointA[0];
	F_Points[5][1] = F_PointB[1];
	F_Points[5][2] = F_PointB[2];

	F_Points[6][0] = F_PointB[0];
	F_Points[6][1] = F_PointA[1];
	F_Points[6][2] = F_PointB[2];

	F_Points[7] = F_PointB;

	switch (I_Color) {
		case 0: { //N Start
			I_Colors = { 0, 255, 0, 255 };
		} case 1: { //N End
			I_Colors = { 255, 0, 0, 255 };
		} case 2: { //N CP
			I_Colors = { 255, 165, 0, 255 };
		} case 3: { //B Start
			I_Colors = { 23, 150, 102, 255 };
		} case 4: { //B End
			I_Colors = { 153, 0, 153, 255 };
		} case 5: { //B CP
			I_Colors = { 200, 100, 0, 255 };
		} case 6: { //Admin Zoning
			I_Colors = { 255, 255, 102, 255 };
		}
	}

	for (int i = 0; i < 4; i++) {
		TE_SetupBeamPoints(F_Points[i], F_Points[i + 4], gH_Models[LaserMaterial], gH_Models[HaloMaterial], 0, 30, F_Display, 2.0, 5.0, 2, 1.0, I_Colors, 0);

		if (B_All) {
			TE_SendToAll();
		} else {
			TE_SendToClient(I_Client);
		}
	}

	for (int i = 0; i < 2; i++) {
		TE_SetupBeamPoints(F_Points[0], F_Points[i + 1], gH_Models[LaserMaterial], gH_Models[HaloMaterial], 0, 30, F_Display, 2.0, 5.0, 2, 1.0, I_Colors, 0);

		if (B_All) {
			TE_SendToAll();
		} else {
			TE_SendToClient(I_Client);
		}
	}

	for (int i = 0; i < 2; i++) {
		TE_SetupBeamPoints(F_Points[3], F_Points[i + 1], gH_Models[LaserMaterial], gH_Models[HaloMaterial], 0, 30, F_Display, 2.0, 5.0, 2, 1.0, I_Colors, 0);

		if (B_All) {
			TE_SendToAll();
		} else {
			TE_SendToClient(I_Client);
		}
	}

	for (int i = 0; i < 2; i++) {
		TE_SetupBeamPoints(F_Points[4], F_Points[i + 5], gH_Models[LaserMaterial], gH_Models[HaloMaterial], 0, 30, F_Display, 2.0, 5.0, 2, 1.0, I_Colors, 0);

		if (B_All) {
			TE_SendToAll();
		} else {
			TE_SendToClient(I_Client);
		}
	}

	for (int i = 0; i < 2; i++) {
		TE_SetupBeamPoints(F_Points[7], F_Points[i + 5], gH_Models[LaserMaterial], gH_Models[HaloMaterial], 0, 30, F_Display, 2.0, 5.0, 2, 1.0, I_Colors, 0);

		if (B_All) {
			TE_SendToAll();
		} else {
			TE_SendToClient(I_Client);
		}
	}
}
