enum ModelsPrecache {
	LaserMaterial,
	HaloMaterial,
	GlowSprite,
	BlueGlowSprite,
	Barrel
};

enum Styles {
	String:StyleName[64],
	String:StylePrefix[16],
	String:Aliases[512],
	String:SpecialId[16],
	bool:Ranked,
	bool:AutoBhop,
	bool:StartBhop,
	Float:Gravity,
	Float:MaxSpeed,
	Fov,
	bool:Sync,
	bool:PreventLeft,
	bool:PreventRight,
	bool:PreventForward,
	bool:PreventBack,
	Float:PreSpeed,
	StyleId,
	Float:ExpMultiplier,
};

enum Titles {
	String:TitleName[64],
	String:TitlePrefix[16],
	String:TitleDesc[512],
	String:CertainMap[64],
	CertainStyle,
	CertainZoneGroup,
	CertainPrestige,
	CertainExp,
	CertainLevel,
	Value,
};

char gC_SettingNames[][] = {
	"Weapon Sounds",
	"Timer Sounds",
	"Show Other Players Records",
	"Show Misc. Timer Messages",
	"Advance Checkpoints",
	"Auto-Hide On Join",
	"Auto-Disable Pickup On Join",
	"Hud Style",
};

char gC_PrestigeShopNames[][][] = {
	{"Connect Messages - WIP", "Display a Public Message whenever you Connect or Disconnect to the server.", "1"},
	{"Join Sounds - WIP", "Play a melody upon Connecting to the server. @Oscar to have your ~5 second clip added.", "2"},
	{"Custom Tags", "Have a Custom Tag (12 char. max) which is shown as a prefix when you type.", "1"},
	{"Trails", "Have a large choice of trails to be used when you are on the server", "2"},
	{"Auras", "Have a large choice of auras to be used when you are on the server", "2"},
}

char gC_PrestigeShopBuyables[][] = {
	"2x Exp (Map)",
	"2x Exp (24 Hours)",
	"2x Exp (7 Days)",
}

char gC_WelcomeMessageShit[][][] = {
	{"Main Page", "Welcome to the Timer Thank you for joining.", "During the Timer's development I was able to meet some great friends and I've learned a lot.", "I hope you appreciate the timer and enjoy your stay in the server.", "This timer was done for you, the user.", "Use 1. and 2. to navigate through the welcome menu.", " \n", "You can return to the menu any time after closing by typing !welcome", "", ""},
	{"Settings", "Settings allow you to alter your experience in the server.", "Too much chat Spam? Disable (Misc. Messages)!", " \n", "Experiment with the settings to find a the right feeling for you!", "", "", "", "", ""},
	{"Seasons", "Seasons are periods of time ranging from 3-6 months", "the current season can impact your Races / Seasonal Exp", "Races & Seasonal Exp are explained further later in this welcome menu", "", "", "", "", "", ""},
	{"Elo", "Elo is generated based off your Map Rank compared to other players", "A player who finished in first place on a map will yield more player than a player in 5th", "Elo gets weighed down per style, therefore to get higher elo, you must compete with higher tiered maps", "Farming lower tier maps will not yield a considerable amount of elo", "Elo is separated into it's respective Ranked Style", "You can be Rank 1 on Auto but rank 10 on Legit, for example.", "", "", ""},
	{"Races", "Races are designed to bring a bigger competitive edge along with Elo", "Players start with 1500 Elo and can race others on any style / map / zonegroup", "You can preview how your elo will be affected if you win/lose before hand", "If you fail to complete (Disconnect) or your opponent finishes before you, you've officially lost", "Elo Decay is in-place for players above 1700 elo", "Any exploitation of Races Elo will result in severe consequences, and elo reverted", "", "", ""},
	{"Seasonal Exp", "Seasonal Exp has been introduced to make obtaining Prestige Tokens easier through utilizing seasons", "After the Season is finished your exp / prestige level will not be carried over, but your prestige tokens ARE!", "The objective is to obtain as many tokens as possible!", "Once you've entered seasonal mode you will gain a 1x Double ExP Booster (7-Day)", "which will be locked to you and that season.", "Highest players will also yield Achievements!", "", "", ""},
	{"Achievements/Titles", "Actions can yield you an Achievement", "Actions such as finishing a map in a certain style or gaining top10 in a season", "they will all yield you a title which you can equip to show off", "Think of it as WoW titles :)", "Pretty cool feature if I say so myself teehee", "", "", "", ""},
	{"Last Words", "Thank you for taking the time to read the Welcome Menu", "I hope you enjoy yourself as much as I did making the Timer", "Shoutouts to the following people:", "balon, zipcore, wraeclaw, Salty", "lastly to The BOYS: SteffyX, Dope, Krelle", "THE BOYS 2k17, 2k17, 2k17 SKRT SKRT BANG BANG", "", "", ""},
}

char gC_AllowedHalos[][] = {
	"76561198187056407",
	"76561198162736480",
	"76561198112699412",
	"76561198059531888",
}

char gC_Blacklist[][] = {
	"76561198083343563",
	"76561198064552644",
	"76561198134328733",
}

char gC_DevList[][] = {
	"76561198187056407",
}

char gC_SpecialTitles[][] = {
	"certain_map",
	"certain_style",
	"certain_zonegroup",
	"certain_prestige",
	"certain_exp",
	"certain_level",
	"certain_mapsdone",
}

char gC_SpecialStyles[][] = {
	"hsw",
	"shsw",
}

char gC_Replays[][] = {
	"Replays",
	"ReplayBackups",
}

float gF_VipExp[4] = {
	0.00,
	0.15,
	0.25,
	0.50,
}

/* SQL */
Database gD_Main;
Database gD_Slave;
Database gD_Log;

/* GLOBALS */
char gC_ServerIp[64], gC_CurrentMap[128];
ConVar gCn_AutoBunnyHopping;

int gI_Styles[TOTAL_STYLES][Styles];
int gI_Titles[TOTAL_TITLES][Titles];

int gI_RainbowColors[3];
int gI_Direction;

bool gB_ServerLoaded;
bool gB_ChangingMaps;

/* LOG */
float F_JoinTimeStamp[MAXPLAYERS + 1];

/* ZONES */
ArrayList gA_Zones;
ArrayList gA_MapRecords[64][TOTAL_STYLES];
ArrayList gA_ReplayFrames[64][TOTAL_STYLES];
int gI_TimerReplayRecords[64][TOTAL_STYLES][2];
//0 - time
//1 - uid

ArrayList gA_ReplayCloneFrames[TOTAL_BOTS];
ArrayList gA_ReplayQueue;
int gI_ReplayBotIds[TOTAL_BOTS];
int gI_ReplayBotTicks[TOTAL_BOTS];
int gI_ReplayCurrentlyReplaying[TOTAL_BOTS - 1][2];

ArrayList gA_SqlQueries;

ArrayList gA_SteamIds;
ArrayList gA_Names;

ArrayList gA_Whitelists[3];

ArrayList gA_Items[2][2];
int gI_TimerPlayerItems[MAXPLAYERS + 1][3];

Handle gH_Models[ModelsPrecache];
Handle gH_ZoneTimer;
Handle gH_SecondTimer;

int gI_CheckpointTotalCount[64];
int gI_MapInfo[64][2];
int gI_TotalZoneGroups;
int gI_TotalStyles;

int gI_ZoneTemp;
float gF_ZoneSpawns[64][64][3];

/* ADMIN */
Handle gH_AddZonesTimer[MAXPLAYERS + 1];
int gI_TimerMiscMultipleSettings[MAXPLAYERS + 1][64];
float gF_AddZonesTemp[MAXPLAYERS + 1][2][3];

/* PLAYER */
ArrayList gA_TimerCheckpointData[MAXPLAYERS + 1];
ArrayList gA_TimerPlayerFrames[MAXPLAYERS + 1];
ArrayList gA_TimerJumpStats[MAXPLAYERS + 1];

char gC_TimerPlayerPrestigeShopValues[MAXPLAYERS + 1][5][512];
char gC_TimerPlayerSteamId[MAXPLAYERS + 1][64];
char gC_TimerPlayerIp[MAXPLAYERS + 1][64];

int gI_TimerPlayerUid[MAXPLAYERS + 1];
int gI_TimerPlayerFrameTicks[MAXPLAYERS + 1];

float gF_TimerTimerPause[MAXPLAYERS + 1][3][3];
float gF_TimerTimerPauseTime[MAXPLAYERS + 1];

bool gB_TimerTimerPaused[MAXPLAYERS + 1];
bool gB_TimerTimerNoclip[MAXPLAYERS + 1];

int gI_TimerCurrentZone[MAXPLAYERS + 1];
int gI_TimerCurrentZoneGroup[MAXPLAYERS + 1];
int gI_TimerCurrentCheckpointStage[MAXPLAYERS + 1];
int gI_TimerCurrentStyle[MAXPLAYERS + 1];
int gI_TimerCurrentTokens[MAXPLAYERS + 1];
int gI_TimerCurrentSeason[MAXPLAYERS + 1];

int gI_TimerCurrentJumps[MAXPLAYERS + 1];
int gI_TimerCurrentJumpInterval[MAXPLAYERS + 1];
bool gB_TimerCurrentJumpSettings[MAXPLAYERS + 1][1];

int gI_PlayerReplay[MAXPLAYERS + 1][2];

int gI_TimerCurrentSync[MAXPLAYERS + 1][2];

float gF_TimerCurrentSpeed[MAXPLAYERS + 1];
float gF_TimerLastAngles[MAXPLAYERS + 1][3];

int gI_TimerPlayerAttempts[MAXPLAYERS + 1][SEASON + 1][64][TOTAL_STYLES];
int gI_TimerPlayerExp[MAXPLAYERS + 1][SEASON + 1][3];

int gI_TimerPlayerLevel[MAXPLAYERS + 1];
int gI_TimerPlayerSettings[MAXPLAYERS + 1][TOTAL_SETTINGS];

float gF_TimerPlayerEloValue[MAXPLAYERS + 1][TOTAL_STYLES];

bool gB_TimerPlayerHide[MAXPLAYERS + 1];
bool gB_TimerPlayerDisablePickup[MAXPLAYERS + 1];
bool gB_TimerPlayerNew[MAXPLAYERS + 1];

int gI_TimerPlayerRecords[MAXPLAYERS + 1][64][TOTAL_STYLES];
int gI_TimerPlayerCheckpointRecords[MAXPLAYERS + 1][64][TOTAL_STYLES][64];

int gI_TimerPlayerElo[MAXPLAYERS + 1][TOTAL_STYLES];
int gI_TimerPlayerVip[MAXPLAYERS + 1][2];
int gI_TimerPlayerRaces[MAXPLAYERS + 1][SEASON + 1][3];

float gF_TimerStartTime[MAXPLAYERS + 1];
float gF_TimerCheckpointTime[MAXPLAYERS + 1];

bool gB_TimerInBonus[MAXPLAYERS + 1];
bool gB_TimerTimerStarted[MAXPLAYERS + 1]
bool gB_TimerRecentlyAbused[MAXPLAYERS + 1];
bool gB_TimerPlayerLoaded[MAXPLAYERS + 1];
