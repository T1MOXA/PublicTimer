public void replay_PlayerCmd(int I_Client, float F_Angles[3], int &I_Buttons) {
	float F_Pos[3];
	int I_CurrentTime;

	GetClientAbsOrigin(I_Client, F_Pos);

	if (IsValidClient(I_Client)) {
		I_CurrentTime = RoundToFloor((GetGameTime() - gF_TimerStartTime[I_Client]) * 1000);

		if (gB_TimerTimerStarted[I_Client] && !gB_TimerTimerPaused[I_Client]) {
			if ((I_CurrentTime < gI_TimerReplayRecords[gI_TimerCurrentZoneGroup[I_Client]][gI_TimerCurrentStyle[I_Client]][0]) || (gI_TimerReplayRecords[gI_TimerCurrentZoneGroup[I_Client]][gI_TimerCurrentStyle[I_Client]][0] == 0)) {
				gA_TimerPlayerFrames[I_Client].Resize(gI_TimerPlayerFrameTicks[I_Client] + 1);

				for (int i = 0; i < 3; i++) {
					gA_TimerPlayerFrames[I_Client].Set(gI_TimerPlayerFrameTicks[I_Client], F_Pos[i], i);
				}

				for (int i = 0; i < 2; i++) {
					gA_TimerPlayerFrames[I_Client].Set(gI_TimerPlayerFrameTicks[I_Client], F_Angles[i], i + 3);
				}

				gA_TimerPlayerFrames[I_Client].Set(gI_TimerPlayerFrameTicks[I_Client], I_Buttons, 5);
				gI_TimerPlayerFrameTicks[I_Client]++;
			}
		}
	}
}

public void replay_Clear(int I_Client) {
	delete gA_TimerPlayerFrames[I_Client];
	gA_TimerPlayerFrames[I_Client] = new ArrayList(6);

	gI_TimerPlayerFrameTicks[I_Client] = 0;
}

public void replay_PluginStart() {
	char C_DirPath[512];

	BuildPath(Path_SM, C_DirPath, 512, "data/TangoTimer");

	if (!DirExists(C_DirPath)) {
		CreateDirectory(C_DirPath, 511);
	}

	for (int i = 0; i < 2; i++) {
		BuildPath(Path_SM, C_DirPath, 512, "data/TangoTimer/%s", gC_Replays[i]);

		if (!DirExists(C_DirPath)) {
			CreateDirectory(C_DirPath, 511);
		}

		for (int x = 0; x < 64; x++) {
			BuildPath(Path_SM, C_DirPath, 512, "data/TangoTimer/%s/%i", gC_Replays[i], x);

			if (!DirExists(C_DirPath)) {
				CreateDirectory(C_DirPath, 511);
			}

			for (int y = 0; y < TOTAL_STYLES; y++) {
				BuildPath(Path_SM, C_DirPath, 512, "data/TangoTimer/%s/%i/%i", gC_Replays[i], x, y);

				if (!DirExists(C_DirPath)) {
					CreateDirectory(C_DirPath, 511);
				}
			}
		}
	}
}

public void replay_LoadAllReplays() {
	char C_DirPath[512], C_Header[512], C_ExplodedHeader[3][512];
	int I_Size;
	any[] A_ReplayData = new any[6];

	for (int i = 0; i < 64; i++) {
		for (int x = 0; x < gI_TotalStyles; x++) {
			gA_ReplayFrames[i][x] = new ArrayList(6);

			BuildPath(Path_SM, C_DirPath, 512, "data/TangoTimer/%s/%i/%i/%s.rec", gC_Replays[0], i, x, gC_CurrentMap);

			if (FileExists(C_DirPath)) {
				File Fi_Replay = OpenFile(C_DirPath, "rb");

				if (!ReadFileLine(Fi_Replay, C_Header, sizeof(C_Header))) {
					return;
				}

				TrimString(C_Header);
				ExplodeString(C_Header, "|", C_ExplodedHeader, 3, 64);

				gI_TimerReplayRecords[i][x][0] = StringToInt(C_ExplodedHeader[2]);
				gI_TimerReplayRecords[i][x][1] = StringToInt(C_ExplodedHeader[0]);
				I_Size = StringToInt(C_ExplodedHeader[1]);

				gA_ReplayFrames[i][x].Resize(I_Size);

				for (int y = 0; y < I_Size; y++) {
					if (ReadFile(Fi_Replay, A_ReplayData, 6, 4) >= 0) {
						for (int z = 0; z < 6; z++) {
							gA_ReplayFrames[i][x].Set(y, view_as<float>(A_ReplayData[z]), z);
							//SetArrayCell(gA_ReplayFrames[i][x], y, view_as<float>(A_ReplayData[z]), z);
						}
					}
				}

				delete Fi_Replay;
			}
		}
	}
}

public void replay_TrySaveReplay(int I_Client, int I_ZoneGroup, int I_Style, int I_Time) {
	char C_DirPath[512], C_CopyPath[512], C_buffer[512];
	int I_Size, I_ArraySize, I_Queued;

	if ((I_Time < gI_TimerReplayRecords[I_ZoneGroup][I_Style][0]) || (gI_TimerReplayRecords[I_ZoneGroup][I_Style][0] == 0)) {
		CloseHandle(gA_ReplayFrames[I_ZoneGroup][I_Style]);

		gA_ReplayFrames[I_ZoneGroup][I_Style] = gA_TimerPlayerFrames[I_Client].Clone();
		gI_TimerReplayRecords[I_ZoneGroup][I_Style][0] = I_Time;
		gI_TimerReplayRecords[I_ZoneGroup][I_Style][1] = gI_TimerPlayerUid[I_Client];

		BuildPath(Path_SM, C_DirPath, 512, "data/TangoTimer/%s/%i/%i/%s.rec", gC_Replays[0], I_ZoneGroup, I_Style, gC_CurrentMap);

		if (FileExists(C_DirPath)) {
			int I_Temp;

			BuildPath(Path_SM, C_CopyPath, 512, "data/TangoTimer/%s/%i/%i/%s-%i.rec", gC_Replays[1], I_ZoneGroup, I_Style, gC_CurrentMap, I_Temp);

			while (FileExists(C_CopyPath)) {
				I_Temp++;
				BuildPath(Path_SM, C_CopyPath, 512, "data/TangoTimer/%s/%i/%i/%s-%i.rec", gC_Replays[1], I_ZoneGroup, I_Style, gC_CurrentMap, I_Temp);
			}

			File_Copy(C_DirPath, C_CopyPath);
			DeleteFile(C_DirPath);
		}

		I_ArraySize = 128 * 6;
		I_Size = gA_ReplayFrames[I_ZoneGroup][I_Style].Length;

		any[] A_ReplayData = new any[I_ArraySize];
		any[] A_FrameData = new any[6];

		File Fi_Replay = OpenFile(C_DirPath, "wb");

		WriteFileLine(Fi_Replay, "%i|%i|%i", gI_TimerPlayerUid[I_Client], I_Size, I_Time);

		for (int i = 0; i < I_Size; i++) {
			GetArrayArray(gA_ReplayFrames[I_ZoneGroup][I_Style], i, A_FrameData, 6);

			for (int x = 0; x < 6; x++) {
				A_ReplayData[((I_Queued * 6) + x)] = A_FrameData[x];
			}

			I_Queued++;

			if(i == (I_Size - 1) || I_Queued == 128) {
				WriteFile(Fi_Replay, A_ReplayData, I_Queued * 6, 4);
				I_Queued = 0;
			}
		}

		if (gI_TimerCurrentZoneGroup[I_Client] == 0) {
			Format(C_buffer, 512, "\x0BN\x01");
		} else {
			Format(C_buffer, 512, "\x0EB%i\x01", gI_TimerCurrentZoneGroup[I_Client]);
		}

		if (gI_TimerPlayerSettings[I_Client][3] == 1) {
			PrintToChat(I_Client, "%s New Replay Bot For [%s] On [\x03%s\x01]", PLUGIN_PREFIX, C_buffer, gI_Styles[I_Style][StylePrefix]);
		}

		delete Fi_Replay;
	}
}

public void replay_ClearBots() {
	for (int i = 0; i < TOTAL_BOTS; i++) {
		delete gA_ReplayCloneFrames[i];
		gA_ReplayCloneFrames[i] = new ArrayList(6);

		gI_ReplayBotIds[i] = -1;
		gI_ReplayBotTicks[i] = -1;
	}

	for (int i = 0; i < TOTAL_BOTS - 1; i++) {
		for (int x = 0; x < 2; x++) {
			gI_ReplayCurrentlyReplaying[i][x] = -1;
		}
	}

	delete gA_ReplayQueue;
	gA_ReplayQueue = new ArrayList(3);

	ServerCommand("bot_kick");
}

public void replay_SpawnBots() {
	replay_ClearBots();

	for (int i = 0; i < TOTAL_BOTS; i++) {
		ServerCommand("bot_add_ct");
	}
}

public void replay_CheckBots() {
	for (int i = 0; i < TOTAL_BOTS; i++) {
		if (gI_ReplayBotIds[i] > 0) {
			if (!IsPlayerAlive(gI_ReplayBotIds[i])) {
				CS_RespawnPlayer(gI_ReplayBotIds[i]);
			}
		}
	}
}

public void replay_TryQueueReplay(int I_Client, int I_ZoneGroup, int I_Style) {
	bool B_Found;
	char C_buffer[512];

	for (int i = 1; i < TOTAL_BOTS; i++) {
		if (gI_ReplayCurrentlyReplaying[i - 1][0] == -1) {
			replay_StartReplay(I_Client, i, I_ZoneGroup, I_Style);

			B_Found = true;
			break;
		}
	}

	if (!B_Found) {
		int I_Length = gA_ReplayQueue.Length;
		gA_ReplayQueue.Resize(I_Length + 1);

		gA_ReplayQueue.Set(I_Length, GetClientUserId(I_Client), 0);
		gA_ReplayQueue.Set(I_Length, I_ZoneGroup, 1);
		gA_ReplayQueue.Set(I_Length, I_Style, 2);

		if (I_ZoneGroup == 0) {
			Format(C_buffer, 512, "\x0BN\x01");
		} else {
			Format(C_buffer, 512, "\x0EB%i\x01", I_ZoneGroup);
		}

		Format(C_buffer, 512, "%s Queued [%s] on [\x03%s\x01] - Pos: #%i", REPLAY_PREFIX, C_buffer, gI_Styles[I_Style][StylePrefix], I_Length + 1);

		if (gI_TimerPlayerSettings[I_Client][3] == 1) {
			PrintToChat(I_Client, C_buffer);
		}
	}
}

public void replay_StartReplay(int I_Client, int I_BotId, int I_ZoneGroup, int I_Style) {
	char C_buffer[512], C_ClientName[16], C_ClanTag[128], C_NameTag[256], C_Name[128], C_Time[128];

	if (IsValidClient(I_Client)) {
		GetClientName(I_Client, C_ClientName, 16);
		TrimString(C_ClientName);
	} else {
		Format(C_ClientName, 16, "D/C'ed");
	}

	gA_ReplayCloneFrames[I_BotId] = gA_ReplayFrames[I_ZoneGroup][I_Style].Clone();

	gI_ReplayCurrentlyReplaying[I_BotId - 1][0] = I_ZoneGroup;
	gI_ReplayCurrentlyReplaying[I_BotId - 1][1] = I_Style;

	gI_ReplayBotTicks[I_BotId] = 0;

	if (I_ZoneGroup == 0) {
		Format(C_buffer, 512, "\x0BN\x01");
	} else {
		Format(C_buffer, 512, "\x0EB%i\x01", I_ZoneGroup);
	}

	Format(C_buffer, 512, "%s Bot: \x0E#%i \x01Started - [%s] on [\x03%s\x01] - \x10%s", REPLAY_PREFIX, I_BotId, C_buffer, gI_Styles[I_Style][StylePrefix], C_ClientName);
	misc_PrintToChatAll(C_buffer, true, 3);

	if (I_ZoneGroup == 0) {
		Format(C_ClanTag, 128, "[N -");
	} else {
		Format(C_ClanTag, 128, "[B%i -", I_ZoneGroup);
	}

	misc_FormatTime(gI_TimerReplayRecords[I_ZoneGroup][I_Style][0], C_Time, 128);
	gA_Names.GetString(gI_TimerReplayRecords[I_ZoneGroup][I_Style][1], C_Name, 128);

	Format(C_ClanTag, 128, "%s %s]", C_ClanTag, gI_Styles[I_Style][StylePrefix]);
	Format(C_NameTag, 256, "%s - %s", C_Time, C_Name);

	CS_SetClientClanTag(gI_ReplayBotIds[I_BotId], C_ClanTag);
	SetClientInfo(gI_ReplayBotIds[I_BotId], "name", C_NameTag);
}
