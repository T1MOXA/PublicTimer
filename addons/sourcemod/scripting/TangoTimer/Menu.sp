public void menu_Season(int I_Client) {
	char C_buffer[512], C_buffer1[512];
	Menu M_Menu = new Menu(SeasonMenuHandle);

	if (gI_TimerCurrentSeason[I_Client] > 0) {
		Format(C_buffer1, 512, "Active - %i", gI_TimerCurrentSeason[I_Client]);
	} else {
		Format(C_buffer1, 512, "In-Active");
	}

	Format(C_buffer, 512, "%s - Seasons\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sSeasonal Mode: %s\n", C_buffer, C_buffer1);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Enter Seasonal Mode");

	if (gI_TimerCurrentSeason[I_Client] > 0) {
		Format(C_buffer, 512, "%s - Already Entered", C_buffer);
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	} else {
		M_Menu.AddItem(C_buffer, C_buffer);
	}

	Format(C_buffer, 512, "See Previous Season Stats");
	M_Menu.AddItem(C_buffer, C_buffer);

	M_Menu.Display(I_Client, 0);
}

public int SeasonMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				menu_SeasonConfirm(I_Param1);
			} case 1: {
				//misc_PreviousSeasonsMenu(I_Param1);
			}
		}
	}
}

public void menu_SeasonConfirm(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(SeasonConfirmMenuHandle);

	Format(C_buffer, 512, "%s - Seasons - Confirm\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sONCE YOU HAVE ENTERED SEASONAL MODE,\n", C_buffer);
	Format(C_buffer, 512, "%sYOU WILL NOT BE ABLE TO LEAVE UNTIL THE SEASON ENDS.\n", C_buffer);
	Format(C_buffer, 512, "%sTHINK ABOUT THIS CAREFULLY!\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "NO! TAKE ME BACK!");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "YES! I WANT TO ENTER SEASONAL MODE!");
	M_Menu.AddItem(C_buffer, C_buffer);

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int SeasonConfirmMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	char C_buffer[512];

	if (I_Param2 == MenuCancel_ExitBack) {
		menu_Season(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				menu_Season(I_Param1);
			} case 1: {
				gI_TimerCurrentSeason[I_Param1] = SEASON;

				Format(C_buffer, 512, "UPDATE `t_players` SET season = '%i' WHERE uid = '%i';", gI_TimerCurrentSeason[I_Param1], gI_TimerPlayerUid[I_Param1]);
				gA_SqlQueries.PushString(C_buffer);

				Format(C_buffer, 512, "INSERT INTO `t_exp` VALUES ('%i', '0', '0', '0', '%i');", gI_TimerPlayerUid[I_Param1], gI_TimerCurrentSeason[I_Param1]);
				gA_SqlQueries.PushString(C_buffer);

				PrintToChat(I_Param1, "%s Seasonal Instance Created ~ Have Fun!", PLUGIN_PREFIX);
				menu_Season(I_Param1);
			}
		}
	}
}

public void menu_Exp(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(ExpMenuHandle);
	int I_NextLevel, I_NextDiff;

	if (gI_TimerPlayerLevel[I_Client] > 99) {
		I_NextLevel = 100;
		I_NextDiff = 0;
	} else {
		I_NextLevel = gI_TimerPlayerLevel[I_Client] + 1;
		I_NextDiff = misc_CalculateExp(I_NextLevel) - gI_TimerPlayerExp[I_Client][gI_TimerCurrentSeason[I_Client]][0];
	}

	Format(C_buffer, 512, "%s - Exp\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sCurrent Level: %i\n", C_buffer, gI_TimerPlayerLevel[I_Client]);
	Format(C_buffer, 512, "%sCurrent Exp: %i\n", C_buffer, gI_TimerPlayerExp[I_Client][gI_TimerCurrentSeason[I_Client]][0]);
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sExp Needed To Level %i: %i\n", C_buffer, I_NextLevel, I_NextDiff);
	Format(C_buffer, 512, "%s \n", C_buffer);

	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Prestige Menu");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Prestige Shop");
	M_Menu.AddItem(C_buffer, C_buffer);

	M_Menu.Display(I_Client, 0);
}

public int ExpMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				menu_Prestige(I_Param1);
			} case 1: {
				menu_PrestigeShop(I_Param1);
			}
		}
	}
}

public void menu_Prestige(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(PrestigeMenuHandle);

	Format(C_buffer, 512, "%s - Prestige Menu\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Prestige Me");

	if (gI_TimerPlayerLevel[I_Client] == 100) {
		M_Menu.AddItem(C_buffer, C_buffer);
	} else {
		Format(C_buffer, 512, "%s - Not Level 100!", C_buffer);
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	}

	Format(C_buffer, 512, "Prestige Shop");
	M_Menu.AddItem(C_buffer, C_buffer);

	M_Menu.Display(I_Client, 0);
}

public int PrestigeMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	char C_buffer[512];

	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				Format(C_buffer, 512, "UPDATE `t_players` SET prestigetokens = prestigetokens + 1 WHERE uid = %i", gI_TimerPlayerUid[I_Param1]);
				gA_SqlQueries.PushString(C_buffer);

				Format(C_buffer, 512, "UPDATE `t_exp` SET currentprestige = currentprestige + 1, currentexp = 0 WHERE uid = %i AND season = %i", gI_TimerPlayerUid[I_Param1], gI_TimerCurrentSeason[I_Param1]);
				gA_SqlQueries.PushString(C_buffer);

				gI_TimerCurrentTokens[I_Param1]++;
				gI_TimerPlayerExp[I_Param1][gI_TimerCurrentSeason[I_Param1]][2]++;
				gI_TimerPlayerExp[I_Param1][gI_TimerCurrentSeason[I_Param1]][0] = 0;

				gI_TimerPlayerLevel[I_Param1] = 0;

				if (gI_TimerPlayerSettings[I_Param1][3] == 1) {
					PrintToChat(I_Param1, "%s Congratulations! You've Prestiged To Prestige: \x10%i", PLUGIN_PREFIX, gI_TimerPlayerExp[I_Param1][gI_TimerCurrentSeason[I_Param1]][2]);
				}
			} case 1: {
				menu_PrestigeShop(I_Param1);
			}
		}
	}
}

public void menu_PrestigeShop(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(PrestigeShopMenuHandle);

	Format(C_buffer, 512, "%s - Prestige Shop", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);

	Format(C_buffer, 512, "%sCurrent Avaliable Prestige Tokens: %i\n", C_buffer, gI_TimerCurrentTokens[I_Client]);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "View Items");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Buy Items");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Refund Items - WIP");
	M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int PrestigeShopMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		menu_Prestige(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				menu_ViewPrestigeItems(I_Param1);
			} case 1: {
				menu_BuyPrestigeItems(I_Param1);
			} case 2: {
				//misc_RefundPrestigeItems(I_Param1);
			}
		}
	}
}

public void menu_ViewPrestigeItems(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(ViewPrestigeItemsHandle);

	Format(C_buffer, 512, "%s - View Prestige Items\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	for (int i = 0; i < sizeof(gC_PrestigeShopNames); i++) {
		M_Menu.AddItem(C_buffer, gC_PrestigeShopNames[i][0]);
	}

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int ViewPrestigeItemsHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		menu_PrestigeShop(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		menu_ViewIndepthPrestigeItem(I_Param1, I_Param2);
	}
}

public void menu_ViewIndepthPrestigeItem(int I_Client, int I_Index) {
	char C_buffer[512];
	Menu M_Menu = new Menu(ViewIndepthPrestigeItemHandle);

	Format(C_buffer, 512, "%s - View Prestige Items\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);

	Format(C_buffer, 512, "%s%s-\n", C_buffer, gC_PrestigeShopNames[I_Index][0]);
	Format(C_buffer, 512, "%s%s\n", C_buffer, gC_PrestigeShopNames[I_Index][1]);
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sPrice: %s Prestige Tokens\n", C_buffer, gC_PrestigeShopNames[I_Index][2]);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	if (gI_TimerPlayerSettings[I_Client][3] == 1) {
		PrintToChat(I_Client, "%s %s - %s", PLUGIN_PREFIX, gC_PrestigeShopNames[I_Index][0], gC_PrestigeShopNames[I_Index][1]);
	}

	Format(C_buffer, 512, "Purchase Item");
	M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int ViewIndepthPrestigeItemHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		menu_ViewPrestigeItems(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		menu_BuyPrestigeItems(I_Param1);
	}
}

public void menu_BuyPrestigeItems(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(BuyPrestigeItemsHandle);

	Format(C_buffer, 512, "%s - Buy Prestige Items\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	for (int i = 0; i < sizeof(gC_PrestigeShopNames); i++) {
		if (strlen(gC_TimerPlayerPrestigeShopValues[I_Client][i]) < 1) {
			M_Menu.AddItem(C_buffer, gC_PrestigeShopNames[i][0]);
		} else {
			M_Menu.AddItem(C_buffer, gC_PrestigeShopNames[i][0], ITEMDRAW_DISABLED);
		}
	}

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int BuyPrestigeItemsHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		menu_PrestigeShop(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		menu_ConfirmBuyPrestigeItem(I_Param1, I_Param2);
	}
}

public void menu_ConfirmBuyPrestigeItem(int I_Client, int I_Item) {
	char C_buffer[512], C_Item[8];
	int I_ItemCost;
	Menu M_Menu = new Menu(ConfirmBuyPrestigeItemHandle);

	I_ItemCost = StringToInt(gC_PrestigeShopNames[I_Item][2]);
	IntToString(I_Item, C_Item, 8);

	Format(C_buffer, 512, "%s - Buy Prestige Item - Confirm\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%s%s\n", C_buffer, gC_PrestigeShopNames[I_Item][0]);
	Format(C_buffer, 512, "%sCost: %s\n", C_buffer, gC_PrestigeShopNames[I_Item][2]);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	if (I_ItemCost <= gI_TimerCurrentTokens[I_Client]) {
		Format(C_buffer, 512, "Buy!");
		M_Menu.AddItem(C_Item, C_buffer);
	} else {
		Format(C_buffer, 512, "Insuficient Funds");
		M_Menu.AddItem(C_Item, C_buffer, ITEMDRAW_DISABLED);
	}

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int ConfirmBuyPrestigeItemHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		menu_BuyPrestigeItems(I_Param1);
		return;
	}

	char C_buffer[512], C_Item[8];
	int I_Item, I_ItemCost;

	if (mA_Action == MenuAction_Select) {
		M_Menu.GetItem(I_Param2, C_Item, 8);
		I_Item = StringToInt(C_Item);

		I_ItemCost = StringToInt(gC_PrestigeShopNames[I_Item][2]);

		Format(C_buffer, 512, "INSERT INTO `t_prestigeshop` VALUES ('%i', '%i', '-1')", gI_TimerPlayerUid[I_Param1], I_Item);
		gA_SqlQueries.PushString(C_buffer);

		Format(C_buffer, 512, "UPDATE `t_players` SET prestigetokens = prestigetokens - '%i' WHERE uid = '%i'", I_ItemCost, gI_TimerPlayerUid[I_Param1]);
		gA_SqlQueries.PushString(C_buffer);

		Format(gC_TimerPlayerPrestigeShopValues[I_Param1][I_Item], 512, "-1");
		gI_TimerCurrentTokens[I_Param1] -= I_ItemCost;

		PrintToChat(I_Param1, "%s Congratulations! You've bought: \x10%s", PLUGIN_PREFIX, gC_PrestigeShopNames[I_Item][0]);
	}
}

public void menu_WelcomeMessage(int I_Client, int I_Index) {
	char C_buffer[2048], C_Index[16];
	Menu M_Menu = new Menu(WelcomeMessageHandle);

	Format(C_buffer, 2048, "Welcome - %s\n", gC_WelcomeMessageShit[I_Index][0]);
	Format(C_buffer, 2048, "%s \n", C_buffer);

	for (int i = 0; i < 64; i++) {
		if (strlen(gC_WelcomeMessageShit[I_Index][i + 1]) < 1) {
			break;
		}

		Format(C_buffer, 2048, "%s%s\n", C_buffer, gC_WelcomeMessageShit[I_Index][i + 1]);
	}

	Format(C_buffer, 2048, "%s \n", C_buffer);

	M_Menu.SetTitle(C_buffer);

	Format(C_Index, 16, "%i", I_Index);

	Format(C_buffer, 2048, "Page Up");
	if (I_Index == 0) {
		M_Menu.AddItem(C_Index, C_buffer, ITEMDRAW_DISABLED);
	} else {
		M_Menu.AddItem(C_Index, C_buffer);
	}

	Format(C_buffer, 2048, "Page Down");
	if (I_Index == sizeof(gC_WelcomeMessageShit) - 1) {
		M_Menu.AddItem(C_Index, C_buffer, ITEMDRAW_DISABLED);
	} else {
		M_Menu.AddItem(C_Index, C_buffer);
	}

	M_Menu.Display(I_Client, 0);

	if (gB_TimerPlayerNew[I_Client]) {
		gB_TimerPlayerNew[I_Client] = false;
	}
}

public int WelcomeMessageHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (mA_Action == MenuAction_Select) {
		char C_Index[16];
		int I_Index;

		M_Menu.GetItem(I_Param2, C_Index, 16);
		I_Index = StringToInt(C_Index);

		switch (I_Param2) {
			case 0: {
			 	menu_WelcomeMessage(I_Param1, I_Index - 1);
			} case 1: {
				menu_WelcomeMessage(I_Param1, I_Index + 1);
			}
		}
	}
}

public void menu_Trails(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(TrailsMenuHandle);

	Format(C_buffer, 512, "%s - Trails\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	int I_Length, I_CurrentTrail;

	I_Length = gA_Items[0][0].Length;
	I_CurrentTrail = StringToInt(gC_TimerPlayerPrestigeShopValues[I_Client][3]);

	Format(C_buffer, 512, "No Trail");

	if (I_CurrentTrail == -1) {
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	} else {
		M_Menu.AddItem(C_buffer, C_buffer);
	}

	for (int i = 0; i < I_Length; i++) {
		gA_Items[0][0].GetString(i, C_buffer, 512);

		if (I_CurrentTrail == i) {
			M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
		} else {
			M_Menu.AddItem(C_buffer, C_buffer);
		}
	}

	M_Menu.Display(I_Client, 0);
}

public int TrailsMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	char C_buffer[512];

	if (mA_Action == MenuAction_Select) {
		int I_Choice = I_Param2 - 1;

		if (I_Param2 == 0) {
			misc_TryKillItem(I_Param1, 0);
		} else {
			misc_GiveParticleItem(I_Param1, I_Choice, 0);
		}

		Format(C_buffer, 512, "UPDATE `t_prestigeshop` SET value = '%i' WHERE uid = '%i' AND item = '3'", I_Choice, gI_TimerPlayerUid[I_Param1]);
		gA_SqlQueries.PushString(C_buffer);

		Format(gC_TimerPlayerPrestigeShopValues[I_Param1][3], 512, "%i", I_Choice);
	}
}

public void menu_Auras(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(AurasMenuHandle);

	Format(C_buffer, 512, "%s - Auras\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	int I_Length, I_CurrentAura;

	I_Length = gA_Items[1][0].Length;
	I_CurrentAura = StringToInt(gC_TimerPlayerPrestigeShopValues[I_Client][4]);

	Format(C_buffer, 512, "No Aura");

	if (I_CurrentAura == -1) {
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	} else {
		M_Menu.AddItem(C_buffer, C_buffer);
	}

	for (int i = 0; i < I_Length; i++) {
		gA_Items[1][0].GetString(i, C_buffer, 512);

		if (I_CurrentAura == i) {
			M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
		} else {
			M_Menu.AddItem(C_buffer, C_buffer);
		}
	}

	M_Menu.Display(I_Client, 0);
}

public int AurasMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	char C_buffer[512];

	if (mA_Action == MenuAction_Select) {
		int I_Choice = I_Param2 - 1;

		if (I_Param2 == 0) {
			misc_TryKillItem(I_Param1, 1);
		} else {
			misc_GiveParticleItem(I_Param1, I_Choice, 1);
		}

		Format(C_buffer, 512, "UPDATE `t_prestigeshop` SET value = '%i' WHERE uid = '%i' AND item = '4'", I_Choice, gI_TimerPlayerUid[I_Param1]);
		gA_SqlQueries.PushString(C_buffer);

		Format(gC_TimerPlayerPrestigeShopValues[I_Param1][4], 512, "%i", I_Choice);
	}
}

public void menu_Settings(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(SettingsMenuHandle);

	Format(C_buffer, 512, "%s - Settings Menu\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	for (int i = 0; i < 7; i++) {
		if (gI_TimerPlayerSettings[I_Client][i] == 1) {
			Format(C_buffer, 512, "%s - [True]", gC_SettingNames[i]);
		} else {
			Format(C_buffer, 512, "%s - [False]", gC_SettingNames[i]);
		}

		M_Menu.AddItem(C_buffer, C_buffer);
	}

	M_Menu.Display(I_Client, 0);
}

public int SettingsMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	char C_buffer[512];

	if (I_Param2 == MenuCancel_Exit) {
		for (int i = 0; i < TOTAL_SETTINGS; i++) {
			Format(C_buffer, 512, "UPDATE `t_settings` SET value = '%i' WHERE uid = '%i' AND setting = '%i'", gI_TimerPlayerSettings[I_Param1][i], gI_TimerPlayerUid[I_Param1], i);
			gA_SqlQueries.PushString(C_buffer);
		}

		return;
	}

	if (mA_Action == MenuAction_Select) {
		if (gI_TimerPlayerSettings[I_Param1][I_Param2] == 1) {
			gI_TimerPlayerSettings[I_Param1][I_Param2] = 0;

			if (gI_TimerPlayerSettings[I_Param1][3] == 1) {
				PrintToChat(I_Param1, "%s %s - \x07False", PLUGIN_PREFIX, gC_SettingNames[I_Param2]);
			}
		} else {
			gI_TimerPlayerSettings[I_Param1][I_Param2] = 1;

			if (gI_TimerPlayerSettings[I_Param1][3] == 1) {
				PrintToChat(I_Param1, "%s %s - \x06True", PLUGIN_PREFIX, gC_SettingNames[I_Param2]);
			}
		}

		menu_Settings(I_Param1);
	}
}

public void menu_MapTop(int I_Client) {
	char C_buffer[512], C_buffer1[512], C_Title[512];
	Menu M_Menu = new Menu(MapTopHandle);

	if (gI_TimerMiscMultipleSettings[I_Client][0] == -1) {
		Format(C_Title, 512, "%s - MapTop - Zone Group", MENU_PREFIX);

		for (int i = 0; i < gI_TotalZoneGroups; i++) {
			if (i == 0) {
				Format(C_buffer, 512, "Normal");
			} else {
				Format(C_buffer, 512, "Bonus %i", i);
			}

			M_Menu.AddItem(C_buffer, C_buffer);
		}

		M_Menu.ExitBackButton = false;
	} else if (gI_TimerMiscMultipleSettings[I_Client][1] == -1) {
		Format(C_Title, 512, "%s - MapTop - Style", MENU_PREFIX);

		for (int i = 0; i < gI_TotalStyles; i++) {
			Format(C_buffer, 512, "%s - %i Records", gI_Styles[i][StyleName], gA_MapRecords[gI_TimerMiscMultipleSettings[I_Client][0]][i].Length);

			if (gA_MapRecords[gI_TimerMiscMultipleSettings[I_Client][0]][i].Length > 0) {
				M_Menu.AddItem(C_buffer, C_buffer);
			} else {
				M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
			}
		}

		M_Menu.ExitBackButton = true;
	} else {
		char C_Time[64], C_Name[128];
		int I_ZoneGroup, I_Style, I_UId, I_Time;

		I_ZoneGroup = gI_TimerMiscMultipleSettings[I_Client][0];
		I_Style = gI_TimerMiscMultipleSettings[I_Client][1];

		Format(C_Title, 512, "%s - MapTop - ZG: %i - %s", MENU_PREFIX, I_ZoneGroup, gI_Styles[I_Style][StyleName]);

		if (gA_MapRecords[I_ZoneGroup][I_Style].Length > 0) {
			for (int i = 0; i < gA_MapRecords[I_ZoneGroup][I_Style].Length; i++) {
				I_Time = gA_MapRecords[I_ZoneGroup][I_Style].Get(i, 0);
				I_UId = gA_MapRecords[I_ZoneGroup][I_Style].Get(i, 1);

				misc_FormatTime(I_Time, C_Time, 64);
				gA_Names.GetString(I_UId, C_Name, 128);

				Format(C_buffer, 512, "[#%i] - %s, %s", i + 1, C_Time, C_Name);
				Format(C_buffer1, 512, "%i", i);
				M_Menu.AddItem(C_buffer1, C_buffer);
			}
		} else {
			Format(C_buffer, 512, "No Records");
			M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
		}

		M_Menu.ExitBackButton = true;
	}

	Format(C_Title, 512, "%s\n", C_Title);
	Format(C_Title, 512, "%s \n", C_Title);
	M_Menu.SetTitle(C_Title);

	M_Menu.Display(I_Client, 0);
}

public int MapTopHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		if (gI_TimerMiscMultipleSettings[I_Param1][1] != -1) {
			gI_TimerMiscMultipleSettings[I_Param1][1] = -1;
		} else if (gI_TimerMiscMultipleSettings[I_Param1][0] != -1) {
			gI_TimerMiscMultipleSettings[I_Param1][0] = -1;
		}

		menu_MapTop(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		if (gI_TimerMiscMultipleSettings[I_Param1][0] == -1) {
			gI_TimerMiscMultipleSettings[I_Param1][0] = I_Param2;

			menu_MapTop(I_Param1);
		} else {
			if (gI_TimerMiscMultipleSettings[I_Param1][1] == -1) {
				gI_TimerMiscMultipleSettings[I_Param1][1] = I_Param2;

				menu_MapTop(I_Param1);
			} else {
				char C_Place[16];
				//int I_Place;

				M_Menu.GetItem(I_Param2, C_Place, 16);
				//I_Place = StringToInt(C_Place);

				PrintToChat(I_Param1, "%s In-Depth Records - WIP", PLUGIN_PREFIX);

				menu_MapTop(I_Param1);
				//misc_IndepthView(gI_PlayerMapTop[I_Param1][0], gI_PlayerMapTop[I_Param1][1], I_Place);
			}
		}
	}
}

public void menu_JumpStats(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(JumpStatsHandle);

	Format(C_buffer, 512, "%s - Jump Stats\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);

	if (gI_TimerCurrentJumpInterval[I_Client] != 0) {
		Format(C_buffer, 512, "%sCurrent Interval: %i\n", C_buffer, gI_TimerCurrentJumpInterval[I_Client]);
	} else {
		Format(C_buffer, 512, "%sCurrent Interval: OFF\n", C_buffer);
	}

	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	if (gI_TimerCurrentJumpInterval[I_Client] == 10) {
		Format(C_buffer, 512, "Limit");
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	} else {
		Format(C_buffer, 512, "+");
		M_Menu.AddItem(C_buffer, C_buffer);
	}

	if (gI_TimerCurrentJumpInterval[I_Client] == 0) {
		Format(C_buffer, 512, "Limit\n \n");
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	} else {
		Format(C_buffer, 512, "-\n \n");
		M_Menu.AddItem(C_buffer, C_buffer);
	}

	Format(C_buffer, 512, "Repeating: %s", (gB_TimerCurrentJumpSettings[I_Client][0] ? "Enabled" : "Disabled"));
	M_Menu.AddItem(C_buffer, C_buffer);

	M_Menu.Display(I_Client, 0);
}

public int JumpStatsHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (mA_Action == MenuAction_Select) {
		if (I_Param2 < 2) {
			switch (I_Param2) {
				case 0: {
					gI_TimerCurrentJumpInterval[I_Param1]++;
				} case 1: {
					gI_TimerCurrentJumpInterval[I_Param1]--;
				}
			}

			if (gI_TimerPlayerSettings[I_Param1][3] == 1) {
				PrintToChat(I_Param1, "%s Jump Interval: \x07%i", PLUGIN_PREFIX, gI_TimerCurrentJumpInterval[I_Param1]);
			}
		} else {
			gB_TimerCurrentJumpSettings[I_Param1][I_Param2 - 2] = !gB_TimerCurrentJumpSettings[I_Param1][I_Param2 - 2];

			if (gI_TimerPlayerSettings[I_Param1][3] == 1) {
				PrintToChat(I_Param1, "%s Repeating: %s", PLUGIN_PREFIX, (gB_TimerCurrentJumpSettings[I_Param1][I_Param2 - 2] ? "\x06Enabled" : "\x07Disabled"));
			}
		}

		menu_JumpStats(I_Param1);
	}
}
