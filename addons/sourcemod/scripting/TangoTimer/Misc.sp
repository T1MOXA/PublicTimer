public void misc_LoadWhitelists() {
	char C_WhitelistPaths[3][512], C_buffer[64];
	int I_Length;
	Handle F_File;

	BuildPath(Path_SM, C_WhitelistPaths[0], 512, "configs/TangoTimer/Zoning.wh");
	BuildPath(Path_SM, C_WhitelistPaths[1], 512, "configs/TangoTimer/Timing.wh");
	BuildPath(Path_SM, C_WhitelistPaths[2], 512, "configs/TangoTimer/Players.wh");

	for (int i = 0; i < 3; i++) {
		if (FileExists(C_WhitelistPaths[i])) {
			F_File = OpenFile(C_WhitelistPaths[i], "r");

			while (!IsEndOfFile(F_File)) {
				ReadFileLine(F_File, C_buffer, 512);

				I_Length = gA_Whitelists[i].Length;

				gA_Whitelists[i].Resize(I_Length + 1);
				gA_Whitelists[i].SetString(I_Length, C_buffer);
			}

			delete F_File;
		}
	}
}

public void misc_LoadStyles() {
	char C_Path[512], C_buffer[512];

	BuildPath(Path_SM, C_Path, 512, "configs/TangoTimer/Styles.cfg");

	KeyValues K_Styles = new KeyValues("Styles");
	K_Styles.ImportFromFile(C_Path);

	K_Styles.JumpToKey("Styles");
	K_Styles.GotoFirstSubKey();

	do {
		char C_SplitString[64][512];
		int I_Split;

		K_Styles.GetString("stylename", gI_Styles[gI_TotalStyles][StyleName], 64);
		K_Styles.GetString("styleprefix", gI_Styles[gI_TotalStyles][StylePrefix], 16);
		K_Styles.GetString("aliases", gI_Styles[gI_TotalStyles][Aliases], 512);
		K_Styles.GetString("specialid", gI_Styles[gI_TotalStyles][SpecialId], 16);

		gI_Styles[gI_TotalStyles][Ranked] = view_as<bool>(K_Styles.GetNum("ranked"));
		gI_Styles[gI_TotalStyles][AutoBhop] = view_as<bool>(K_Styles.GetNum("autobhop"));
		gI_Styles[gI_TotalStyles][StartBhop] = view_as<bool>(K_Styles.GetNum("startbhop"));

		gI_Styles[gI_TotalStyles][Gravity] = K_Styles.GetFloat("gravity");
		gI_Styles[gI_TotalStyles][MaxSpeed] = K_Styles.GetFloat("maxspeed");
		gI_Styles[gI_TotalStyles][Fov] = K_Styles.GetNum("fov");

		gI_Styles[gI_TotalStyles][Sync] = view_as<bool>(K_Styles.GetNum("sync"));

		gI_Styles[gI_TotalStyles][PreventLeft] = view_as<bool>(K_Styles.GetNum("prevent_left"));
		gI_Styles[gI_TotalStyles][PreventRight] = view_as<bool>(K_Styles.GetNum("prevent_right"));
		gI_Styles[gI_TotalStyles][PreventForward] = view_as<bool>(K_Styles.GetNum("prevent_forward"));
		gI_Styles[gI_TotalStyles][PreventBack] = view_as<bool>(K_Styles.GetNum("prevent_back"));

		gI_Styles[gI_TotalStyles][PreSpeed] = K_Styles.GetFloat("prespeed");

		gI_Styles[gI_TotalStyles][StyleId] = gI_TotalStyles;
		gI_Styles[gI_TotalStyles][ExpMultiplier] = K_Styles.GetFloat("expmultiplier");

		ExplodeString(gI_Styles[gI_TotalStyles][Aliases], ",", C_SplitString, 64, 512);

		while (strlen(C_SplitString[I_Split]) > 0) {
			TrimString(C_SplitString[I_Split]);

			Format(C_buffer, 512, "sm_%s", C_SplitString[I_Split]);
			String_ToLower(C_buffer, C_buffer, 512);

			RegConsoleCmd(C_buffer, command_newstyle);
			I_Split++;
		}

		gI_TotalStyles++;
	} while (K_Styles.GotoNextKey())

	delete K_Styles;
}

public void misc_LoadTitles() {
	char C_Path[512];

	BuildPath(Path_SM, C_Path, 512, "configs/TangoTimer/Titles.cfg");

	KeyValues K_Titles = new KeyValues("Titles");
	K_Titles.ImportFromFile(C_Path);

	K_Titles.JumpToKey("Titles");
	K_Titles.GotoFirstSubKey();

	do {
		//char C_buffer[512];
	} while (K_Titles.GotoNextKey())

	delete K_Titles;
}

public void misc_LoadItems() {
	char C_Path[512];

	for (int i = 0; i < 2; i++) {
		delete gA_Items[0][i];
		gA_Items[0][i] = new ArrayList(512);

		delete gA_Items[1][i];
		gA_Items[1][i] = new ArrayList(512);
	}

	BuildPath(Path_SM, C_Path, 512, "configs/TangoTimer/Items.cfg");

	KeyValues K_Items = new KeyValues("Items");
	K_Items.ImportFromFile(C_Path);

	K_Items.JumpToKey("Items");
	K_Items.GotoFirstSubKey();

	do {
		char C_buffer[512];
		int I_Active, I_Type, I_Length;

		/*Type = 0 - Trail
		Type = 1 - Aura*/

		I_Active = K_Items.GetNum("active");

		K_Items.GetString("material", C_buffer, 512);
		misc_DownloadTable(C_buffer);

		if (I_Active > 0) {
			I_Type = K_Items.GetNum("type");

			K_Items.GetString("pcf", C_buffer, 512);
			misc_DownloadTable(C_buffer);

			PrecacheGeneric(C_buffer, true);
			PrecacheParticleEffect(C_buffer);

			I_Length = gA_Items[I_Type][0].Length;

			for (int i = 0; i < 2; i++) {
				gA_Items[I_Type][i].Resize(I_Length + 1);
			}

			K_Items.GetString("desc", C_buffer, 512);
			gA_Items[I_Type][0].SetString(I_Length, C_buffer);

			K_Items.GetString("name", C_buffer, 512);
			gA_Items[I_Type][1].SetString(I_Length, C_buffer);
		}

	} while (K_Items.GotoNextKey())

	PrecacheEffect();

	delete K_Items;
}

public void misc_ShowHud(int I_Client, int I_Target) {
	char C_buffer[4096], C_Time[64], C_WRTime[64], C_PBTime[64], C_Hex[64], C_Stage[64];
	int I_Time, I_iTime, I_Place, I_ZoneGroup, I_Style;
	float F_GameTime;

	I_Time = RoundToFloor((GetGameTime() - gF_TimerStartTime[I_Target]) * 1000);
	F_GameTime = GetGameTime();

	I_ZoneGroup = gI_TimerCurrentZoneGroup[I_Target];
	I_Style = gI_TimerCurrentStyle[I_Target];

	misc_FormatTime(I_Time, C_Time, 64);
	misc_FormatTime(gI_TimerPlayerRecords[0][I_ZoneGroup][I_Style], C_WRTime, 64);
	misc_FormatTime(gI_TimerPlayerRecords[I_Target][I_ZoneGroup][I_Style], C_PBTime, 64);

	if (gA_MapRecords[I_ZoneGroup][I_Style] != INVALID_HANDLE) {
		for (int i = 0; i < gA_MapRecords[I_ZoneGroup][I_Style].Length; i++) {
			I_iTime = gA_MapRecords[I_ZoneGroup][I_Style].Get(i);

			if (I_Time < I_iTime) {
				I_Place = i;
				break;
			}

			if (i == gA_MapRecords[I_ZoneGroup][I_Style].Length - 1) {
				I_Place = i + 1;
				break;
			}
		}
	}

	I_Place++;

	if (gI_MapInfo[gI_TimerCurrentZoneGroup[I_Target]][1] == 1) {
		FormatEx(C_Stage, 64, "Linear");
	} else {
		FormatEx(C_Stage, 64, "%i/%i", gI_TimerCurrentCheckpointStage[I_Target] + 1, gI_CheckpointTotalCount[gI_TimerCurrentZoneGroup[I_Target]] + 1);
	}

	FormatEx(C_Hex, 64, "#%02X%02X%02X", gI_RainbowColors[0], gI_RainbowColors[1], gI_RainbowColors[2]);
	FormatEx(C_buffer, 4096, "\t<font size='20'><font color='#FFFFFF'>Instinct<font color='%s'>Timer</font></font><b>\n", C_Hex);

	FormatEx(C_buffer, 4096, "%s<font face='Arial' size='15'>", C_buffer)

	if (gB_TimerTimerStarted[I_Target]) {
		if ((F_GameTime - gF_TimerCheckpointTime[I_Target]) < 3 && (gF_TimerCheckpointTime[I_Target] != 0)) {
			int I_CPWRTime, I_CPPBTime, I_CPWRDiff, I_CPPBDiff;

			I_Time = gA_TimerCheckpointData[I_Target].Get(gI_TimerCurrentCheckpointStage[I_Target] - 1);

			I_CPWRTime = gI_TimerPlayerCheckpointRecords[0][I_ZoneGroup][I_Style][gI_TimerCurrentCheckpointStage[I_Target] - 1];
			I_CPPBTime = gI_TimerPlayerCheckpointRecords[I_Target][I_ZoneGroup][I_Style][gI_TimerCurrentCheckpointStage[I_Target] - 1];

			I_CPWRDiff = I_CPWRTime - I_Time;
			I_CPPBDiff = I_CPPBTime - I_Time;

			misc_FormatTime(I_CPWRDiff, C_WRTime, 64);
			misc_FormatTime(I_CPPBDiff, C_PBTime, 64);

			misc_FormatPrefixHudTime(I_CPWRTime, I_CPWRDiff, C_WRTime, 64);
			misc_FormatPrefixHudTime(I_CPPBTime, I_CPPBDiff, C_PBTime, 64);
		}

		if (!gB_TimerTimerPaused[I_Target]) {
			FormatEx(C_buffer, 4096, "%sTime: %s\t#%i", C_buffer, C_Time, I_Place);
		} else {
			FormatEx(C_buffer, 4096, "%sTime: Paused\t", C_buffer);
		}

		FormatEx(C_buffer, 4096, "%s\tPB: %s\n", C_buffer, C_PBTime);

		FormatEx(C_buffer, 4096, "%sSpeed: %03.0f\t\tWR: %s\n", C_buffer, gF_TimerCurrentSpeed[I_Target], C_WRTime);
		FormatEx(C_buffer, 4096, "%sSync: %02.2f\t\tStage: %s", C_buffer, (float(gI_TimerCurrentSync[I_Target][0] + 1) / float(gI_TimerCurrentSync[I_Target][1] + 1) * 100.0), C_Stage);
	} else {
		if (gI_TimerCurrentZone[I_Target] == 0) {
			FormatEx(C_buffer, 4096, "%sNormal Start Zone\t", C_buffer);
			FormatEx(C_buffer, 4096, "%sPB: %s\n", C_buffer, C_PBTime);

			FormatEx(C_buffer, 4096, "%sSpeed: %03.0f\t\tWR: %s\n", C_buffer, gF_TimerCurrentSpeed[I_Target], C_WRTime);
			FormatEx(C_buffer, 4096, "%sStyle: %s", C_buffer, gI_Styles[I_Style][StyleName]);
		} else if (gI_TimerCurrentZone[I_Target] == 3) {
			FormatEx(C_buffer, 4096, "%sBonus %i Start Zone\t", C_buffer, I_ZoneGroup);
			FormatEx(C_buffer, 4096, "%sPB: %s\n", C_buffer, C_PBTime);

			FormatEx(C_buffer, 4096, "%sSpeed: %03.0f\t\tWR: %s\n", C_buffer, gF_TimerCurrentSpeed[I_Target], C_WRTime);
			FormatEx(C_buffer, 4096, "%sStyle: %s", C_buffer, gI_Styles[I_Style][StyleName]);
		} else {
			FormatEx(C_buffer, 4096, "%sTime: Stopped\t", C_buffer);
			FormatEx(C_buffer, 4096, "%s\tPB: %s\n", C_buffer, C_PBTime);

			FormatEx(C_buffer, 4096, "%sSpeed: %03.0f\t\tWR: %s\n", C_buffer, gF_TimerCurrentSpeed[I_Target], C_WRTime);
			FormatEx(C_buffer, 4096, "%sStyle: %s", C_buffer, gI_Styles[I_Style][StyleName]);
		}
	}

	PrintHintText(I_Client, C_buffer);
}

public void misc_DownloadTable(char[] C_FileName) {
	AddFileToDownloadsTable(C_FileName);

	int I_Replaces = ReplaceString(C_FileName, 512, ".vmt", ".vtf", true);

	if (I_Replaces > 0) {
		AddFileToDownloadsTable(C_FileName);
	}
}

public bool misc_IsWhitelisted(int I_Client, int I_WhitelistType) {
	/* I_WhitelistType
		0 - Zoning Whitelist
		1 - Time Whitelist
		2 - Player Whitelist
	*/
	char C_buffer[512];

	for (int i = 0; i < gA_Whitelists[I_WhitelistType].Length; i++) {
		gA_Whitelists[I_WhitelistType].GetString(i, C_buffer, 512);

		if (StrContains(C_buffer, gC_TimerPlayerSteamId[I_Client], false) > -1) {
			return true;
		}
	}

	return false;
}

public void misc_PrintToChatSpectators(int I_Client, char[] C_Message) {
	for (int i = 0; i <= MaxClients; i++) {
		if (IsValidClient(i)) {
			if (IsClientObserver(i)) {
				int I_ClientSpecMode = GetEntProp(i, Prop_Send, "m_iObserverMode");

				if (I_ClientSpecMode == 4 || I_ClientSpecMode == 5) {
					int I_Target = GetEntPropEnt(i, Prop_Send, "m_hObserverTarget");

					if (I_Client == I_Target) {
						PrintToChat(i, C_Message);
					}
				}
			}
		}
	}
}

public void misc_PrintToConsoleSpectators(int I_Client, char[] C_Message) {
	for (int i = 1; i <= MaxClients; i++) {
		if (IsValidClient(i)) {
			if (IsClientObserver(i)) {
				int I_ClientSpecMode = GetEntProp(i, Prop_Send, "m_iObserverMode");

				if (I_ClientSpecMode == 4 || I_ClientSpecMode == 5) {
					int I_Target = GetEntPropEnt(i, Prop_Send, "m_hObserverTarget");

					if (I_Client == I_Target) {
						PrintToConsole(i, C_Message);
					}
				}
			}
		}
	}
}

public void misc_TeleportClientToZone(int I_Client, int I_ZoneGroup, int I_CheckpointID, int I_Type) {
	char C_ZoneName[64], C_CheckpointName[64];
	int I_ZoneType, I_Style;

	misc_FormatZoneName(I_ZoneGroup, C_ZoneName, 64);
	misc_FormatCheckpointName(I_CheckpointID, I_ZoneGroup, C_CheckpointName, 64);

	I_Style = gI_TimerCurrentStyle[I_Client];

	if (!IsPlayerAlive(I_Client)) {
		CS_RespawnPlayer(I_Client);
	}

	switch (I_CheckpointID) {
		case 0: {
			I_ZoneType = 0;
		} case 63: {
			I_ZoneType = 1;
		} default: {
			I_ZoneType = 99;
		}
	}

	if (I_ZoneGroup > 0) {
		I_ZoneType += 3;
	}

	if (gF_ZoneSpawns[I_ZoneGroup][I_CheckpointID][0] == 0.0) {
		PrintToChat(I_Client, "%s No %s For %s", PLUGIN_PREFIX, C_CheckpointName, C_ZoneName);
	} else {
		if ((gI_TimerCurrentZoneGroup[I_Client] == I_ZoneGroup) && (gI_TimerCurrentZone[I_Client] == I_ZoneType) && (I_Type != 1)) {
			if (gI_TimerPlayerSettings[I_Client][3] == 1) {
				PrintToChat(I_Client, "%s You're Already In %s For %s", PLUGIN_PREFIX, C_CheckpointName, C_ZoneName);
			}
		} else {
			timer_StopTime(I_Client);
			misc_RecentlyAbused(I_Client, 0.5);

			if (gI_Styles[I_Style][AutoBhop]) {
				SendConVarValue(I_Client, gCn_AutoBunnyHopping, "1");
			} else {
				SendConVarValue(I_Client, gCn_AutoBunnyHopping, "0");
			}

			SetEntProp(I_Client, Prop_Send, "m_iFOV", gI_Styles[I_Style][Fov]);
			SetEntProp(I_Client, Prop_Send, "m_iDefaultFOV", gI_Styles[I_Style][Fov]);

			SetEntityGravity(I_Client, (1.0 - gI_Styles[I_Style][Gravity]));
			SetEntPropFloat(I_Client, Prop_Send, "m_flLaggedMovementValue", 1.0);

			TeleportEntity(I_Client, gF_ZoneSpawns[I_ZoneGroup][I_CheckpointID], NULL_VECTOR, view_as<float>({0.0, 0.0, 0.0}));

			if (I_Type != 1) {
				if (gI_TimerPlayerSettings[I_Client][3] == 1) {
					PrintToChat(I_Client, "%s Teleported To %s For %s", PLUGIN_PREFIX, C_CheckpointName, C_ZoneName);
				}
			}
		}
	}
}

public void misc_DelayedTeleportClientToZone(int I_Client, int I_ZoneGroup, int I_CheckpointID, int I_Type) {
	DataPack D_Pack = new DataPack();

	D_Pack.WriteCell(GetClientUserId(I_Client));
	D_Pack.WriteCell(I_ZoneGroup);
	D_Pack.WriteCell(I_CheckpointID);
	D_Pack.WriteCell(I_Type);

	if (GetClientTeam(I_Client) != 2) {
		ChangeClientTeam(I_Client, 2);
	}

	CreateTimer(0.001, Timer_TeleportClientZone, D_Pack);
}

public void misc_FormatTime(int I_Time, char[] C_buffer, int I_MaxLength) {
	int I_Mins, I_Secs, I_Mili;
	I_Mili = I_Time;

	if (I_Mili < 0)
		I_Mili *= -1;

	if (I_Mili >= 60000)
	{
		I_Mins = RoundToFloor(I_Mili / 60000.0);
		I_Mili = I_Mili % 60000;
	}

	if (I_Mili >= 1000)
	{
		I_Secs = RoundToFloor(I_Mili / 1000.0);
		I_Mili = I_Mili % 1000;
	}

	Format(C_buffer, I_MaxLength, "%02d:%02d.%03d", I_Mins, I_Secs, I_Mili);
}

public void misc_FormatPrefixTime(int I_Time, int I_Diff, char[] C_buffer, int I_MaxLength) {
	if (I_Time == 0) {
		Format(C_buffer, I_MaxLength, "\x0A~%s\x01", C_buffer);
	} else if (I_Diff == 0) {
		Format(C_buffer, I_MaxLength, "\x10=%s\x01", C_buffer);
	} else if (I_Diff > 0) {
		Format(C_buffer, I_MaxLength, "\x06+%s\x01", C_buffer);
	} else {
		Format(C_buffer, I_MaxLength, "\x07-%s\x01", C_buffer);
	}
}

public void misc_FormatPrefixHudTime(int I_Time, int I_Diff, char[] C_buffer, int I_MaxLength) {
	if (I_Time == 0) {
		Format(C_buffer, I_MaxLength, "<font color='#929292'>~%s</font>", C_buffer);
	} else if (I_Diff == 0) {
		Format(C_buffer, I_MaxLength, "<font color='#F5970E'>=%s</font>", C_buffer);
	} else if (I_Diff > 0) {
		Format(C_buffer, I_MaxLength, "<font color='#1EF753'>+%s</font>", C_buffer);
	} else {
		Format(C_buffer, I_MaxLength, "<font color='#FE3636'>-%s</font>", C_buffer);
	}
}

public void misc_FormatZoneName(int I_ZoneGroup, char[] C_buffer, int I_MaxLength) {
	if (I_ZoneGroup == 0) {
		Format(C_buffer, 64, "\x0BNormal\x01")
	} else {
		Format(C_buffer, 64, "\x0EBonus \x01%i", I_ZoneGroup);
	}
}

public void misc_FormatCheckpointName(int I_CheckpointID, int I_ZoneGroup, char[] C_buffer, int I_MaxLength) {
	if (I_CheckpointID == 0) {
		Format(C_buffer, 64, "\x06Start\x01");
	} else if (I_CheckpointID == 63) {
		Format(C_buffer, 64, "\x07End\x01");
	} else {
		if (gI_MapInfo[I_ZoneGroup][1] == 1) {
			Format(C_buffer, 64, "\x10Checkpoint\x01: %i", I_CheckpointID);
		} else {
			Format(C_buffer, 64, "\x10Stage\x01: %i", I_CheckpointID);
		}
	}
}

public void misc_RecentlyAbused(int I_Client, float F_Value) {
	gB_TimerRecentlyAbused[I_Client] = true;

	CreateTimer(F_Value, Timer_RecentlyAbused, GetClientUserId(I_Client));
}

public void misc_TryGiveItem(int I_Client, char[] C_ItemName, int I_Slot) {
	int I_WeaponIndex;

	if (!IsPlayerAlive(I_Client)) {
		PrintToChat(I_Client, "%s You're Dead", PLUGIN_PREFIX);
	} else {
		I_WeaponIndex = GetPlayerWeaponSlot(I_Client, I_Slot);

		if (I_WeaponIndex != -1) {
			RemovePlayerItem(I_Client, I_WeaponIndex);
			RemoveEdict(I_WeaponIndex);
		}

		GivePlayerItem(I_Client, C_ItemName);
		PrintToChat(I_Client, "%s Received \"\x06%s\x01\"", PLUGIN_PREFIX, C_ItemName);
	}
}

public void misc_Message(int I_Client, int I_ZoneGroup, int I_Style, int I_Jumps, int I_Time, int I_PBTime, int I_WRTime, int I_CheckpointStage, int I_Type) {
	char C_buffer[512], C_ClientName[12], C_ZoneName[16], C_Time[32], C_PBTime[32], C_WRTime[32], C_PBDiff[32], C_WRDiff[32], C_CP[32];
	int I_WRDiff, I_PBDiff, I_ClientRank, I_TotalRanks;

	I_PBDiff = I_PBTime - I_Time;
	I_WRDiff = I_WRTime - I_Time;

	misc_FormatTime(I_Time, C_Time, 32);
	misc_FormatTime(I_PBTime, C_PBTime, 32);
	misc_FormatTime(I_WRTime, C_WRTime, 32);

	misc_FormatTime(I_PBDiff, C_PBDiff, 32);
	misc_FormatTime(I_WRDiff, C_WRDiff, 32);

	misc_FormatPrefixTime(I_PBTime, I_PBDiff, C_PBDiff, 32);
	misc_FormatPrefixTime(I_WRTime, I_WRDiff, C_WRDiff, 32);

	GetClientName(I_Client, C_ClientName, 12);
	TrimString(C_ClientName);

	I_ClientRank = gA_MapRecords[I_ZoneGroup][I_Style].FindValue(gI_TimerPlayerUid[I_Client], 1) + 1;
	I_TotalRanks = gA_MapRecords[I_ZoneGroup][I_Style].Length;

	if (I_ZoneGroup == 0) {
		Format(C_ZoneName, 16, "\x0BN\x01");
	} else {
		Format(C_ZoneName, 16, "\x0EB%i\x01", I_ZoneGroup);
	}

	if (I_Type == 0) {
		if (gI_MapInfo[I_ZoneGroup][1] != 0) {
			Format(C_CP, 32, "\x0DCP: \x10%i", I_CheckpointStage);
		} else {
			Format(C_CP, 32, "\x0DStage: \x10%i", I_CheckpointStage);
		}

		Format(C_buffer, 512, "%s %s \x01T: %s", PLUGIN_PREFIX, C_CP, C_Time);
		misc_PrintToChat(I_Client, C_buffer, false, 0, true, true, true);
	} else {
		if (I_WRDiff > 0 || I_WRTime == 0) {
			Format(C_buffer, 512, " \x10New Server Style Record");
			misc_PrintToChatAll(C_buffer, true, 2);
			misc_PrintToChatAll(C_buffer, true, 2);
		}

		Format(C_buffer, 512, "%s Finished [%s] on [\x03%s\x01] - T: %s (\x04%i\x01/\x07%i\x01)", PLUGIN_PREFIX, C_ZoneName, gI_Styles[I_Style][StylePrefix], C_Time, I_ClientRank, I_TotalRanks);
		PrintToChat(I_Client, C_buffer);

		Format(C_buffer, 512, "%s \x10%s\x01 Finished [%s] on [\x03%s\x01] - T: %s (\x04%i\x01/\x07%i\x01)", PLUGIN_PREFIX, C_ClientName, C_ZoneName, gI_Styles[I_Style][StylePrefix], C_Time, I_ClientRank, I_TotalRanks);
		misc_PrintToChat(I_Client, C_buffer, true, 2, false, false, false);
	}

	Format(C_buffer, 512, " %s (PB: \x0B%s\x01) | %s (WR: \x0B%s\x01)", C_PBDiff, C_PBTime, C_WRDiff, C_WRTime);
	misc_PrintToChat(I_Client, C_buffer, true, 4, true, true, false);
}

public void misc_PrintToChat(int I_Client, char[] C_Message, bool B_SettingsCheck, int I_Setting, bool B_OriginalClient, bool B_NeededSpec, bool B_Suffix) {
	char C_buffer[512], C_ClientName[16];

	if (B_OriginalClient) {
		if (B_SettingsCheck) {
			if (gI_TimerPlayerSettings[I_Client][I_Setting] == 1) {
				PrintToChat(I_Client, C_Message);
			}
		} else {
			PrintToChat(I_Client, C_Message);
		}
	}

	GetClientName(I_Client, C_ClientName, 16);

	if (B_Suffix) {
		Format(C_buffer, 512, "%s - %s", C_Message, C_ClientName);
	} else {
		Format(C_buffer, 512, "%s", C_Message);
	}

	for (int i = 1; i <= MaxClients; i++) {
		if (IsValidClient(i) && (i != I_Client)) {
			if (B_NeededSpec) {
				if (IsClientObserver(i)) {
					int I_ClientSpecMode = GetEntProp(i, Prop_Send, "m_iObserverMode");

					if (I_ClientSpecMode == 4 || I_ClientSpecMode == 5) {
						int I_Target = GetEntPropEnt(i, Prop_Send, "m_hObserverTarget");

						if (I_Client == I_Target) {
							if (B_SettingsCheck) {
								if (gI_TimerPlayerSettings[i][I_Setting] == 1) {
									PrintToChat(i, C_buffer);
								}
							} else {
								PrintToChat(i, C_buffer);
							}
						}
					}
				}
			} else {
				if (B_SettingsCheck) {
					if (gI_TimerPlayerSettings[i][I_Setting] == 1) {
						PrintToChat(i, C_buffer);
					}
				} else {
					PrintToChat(i, C_buffer);
				}
			}
		}
	}
}

public void misc_PrintToChatAll(char[] C_Message, bool B_SettingsCheck, int I_Setting) {
	for (int i = 1; i <= MaxClients; i++) {
		if (IsValidClient(i)) {
			if (B_SettingsCheck) {
				if (gI_TimerPlayerSettings[i][I_Setting] == 1) {
					PrintToChat(i, C_Message);
				}
			} else {
				PrintToChat(i, C_Message);
			}
		}
	}
}

public void misc_StyleMenu(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(StyleMenuHandle);

	Format(C_buffer, 512, "%s - Styles\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	for (int i = 0; i < gI_TotalStyles; i++) {
		Format(C_buffer, 512, "%s", gI_Styles[i][StyleName]);
		M_Menu.AddItem(C_buffer, C_buffer);
	}

	M_Menu.Display(I_Client, 0);
}

public int StyleMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (mA_Action == MenuAction_Select) {
		gI_TimerCurrentStyle[I_Param1] = I_Param2;

		misc_DelayedTeleportClientToZone(I_Param1, gI_TimerCurrentZoneGroup[I_Param1], 0, 1);
		PrintToChat(I_Param1, "%s Style Now: \x10%s", PLUGIN_PREFIX, gI_Styles[I_Param2][StyleName]);
	}
}

public void misc_FixPushTrigger(int I_UserId, bool B_Up) {
	float F_Pos[3], F_Vel[3];
	int I_Client;

	I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		GetClientAbsOrigin(I_Client, F_Pos);
		GetEntPropVector(I_Client, Prop_Data, "m_vecAbsVelocity", F_Vel);

		if (B_Up) {
			F_Vel[2] += 5.0;
		} else {
			F_Vel[2] -= 5.0;
		}

		TeleportEntity(I_Client, F_Pos, NULL_VECTOR, F_Vel);

		if (B_Up) {
			CreateTimer(0.5, Timer_FixPushTrigger, I_UserId);
		}
	}
}

public void misc_CheckSpawnPoints() {
	char C_Classname[128];

	if (gF_ZoneSpawns[0][0][0] != 0.0) {
		for (int i = 0; i < GetMaxEntities(); i++) {
			if (IsValidEntity(i) && IsValidEdict(i) && GetEdictClassname(i, C_Classname, 128)) {
				if (StrEqual(C_Classname, "info_player_terrorist") || StrEqual(C_Classname, "info_player_counterterrorist")) {
					AcceptEntityInput(i, "Kill");
				}
			}
		}

		for (int i = 0; i < 64; i++) {
			for (int x = 0; x < 2; x++) {
				int I_SpawnPoint = CreateEntityByName((x == 1) ? "info_player_terrorist":"info_player_counterterrorist");

				if (DispatchSpawn(I_SpawnPoint)) {
					TeleportEntity(I_SpawnPoint, gF_ZoneSpawns[0][0], NULL_VECTOR, NULL_VECTOR);
				}
			}
		}
	}
}

public void misc_AlterLocalDB(int I_Client, int I_Time, float F_Sync, int I_Jumps, int I_Style, int I_ZoneGroup, int I_TimeStamp) {
	int I_Find = gA_MapRecords[I_ZoneGroup][I_Style].FindValue(gI_TimerPlayerUid[I_Client], 1);

	if (I_Find != -1) {
		gA_MapRecords[I_ZoneGroup][I_Style].Erase(I_Find);
	}

	if (gA_MapRecords[I_ZoneGroup][I_Style].Length == 0) {
		gA_MapRecords[I_ZoneGroup][I_Style].Resize(1);

		gA_MapRecords[I_ZoneGroup][I_Style].Set(0, I_Time, 0);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(0, gI_TimerPlayerUid[I_Client], 1);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(0, RoundToFloor(F_Sync * 100.0), 2);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(0, I_Jumps, 3);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(0, I_Style, 4);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(0, I_ZoneGroup, 5);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(0, I_TimeStamp, 6);
	} else {
		for (int i = 0; i < gA_MapRecords[I_ZoneGroup][I_Style].Length + 1; i++) {
			bool B_Break;

			if (i < gA_MapRecords[I_ZoneGroup][I_Style].Length) {
				int I_iTime = gA_MapRecords[I_ZoneGroup][I_Style].Get(i, 0);

				if (I_Time < I_iTime) {
					gA_MapRecords[I_ZoneGroup][I_Style].ShiftUp(i);
					B_Break = true;
				}
			} else {
				gA_MapRecords[I_ZoneGroup][I_Style].Resize(gA_MapRecords[I_ZoneGroup][I_Style].Length + 1);
				B_Break = true;
			}

			if (B_Break) {
				gA_MapRecords[I_ZoneGroup][I_Style].Set(i, I_Time, 0);
				gA_MapRecords[I_ZoneGroup][I_Style].Set(i, gI_TimerPlayerUid[I_Client], 1);
				gA_MapRecords[I_ZoneGroup][I_Style].Set(i, RoundToFloor(F_Sync * 100.0), 2);
				gA_MapRecords[I_ZoneGroup][I_Style].Set(i, I_Jumps, 3);
				gA_MapRecords[I_ZoneGroup][I_Style].Set(i, I_Style, 4);
				gA_MapRecords[I_ZoneGroup][I_Style].Set(i, I_ZoneGroup, 5);
				gA_MapRecords[I_ZoneGroup][I_Style].Set(i, I_TimeStamp, 6);

				break;
			}
		}
	}
}

public void misc_ReplayMenu(int I_Client) {
	char C_buffer[512], C_ZoneGroupName[64], C_Time[64], C_PlayerName[128];
	Menu M_Menu = new Menu(ReplayMenuHandle);

	Format(C_buffer, 512, "%s - Replay\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	if (gI_PlayerReplay[I_Client][0] == 0) {
		Format(C_ZoneGroupName, 64, "Normal");
	} else {
		Format(C_ZoneGroupName, 64, "Bonus: %i", gI_PlayerReplay[I_Client][0]);
	}

	Format(C_buffer, 512, "Zone Group: Up\n");
	Format(C_buffer, 512, "%s > Current ZoneGroup: %s", C_buffer, C_ZoneGroupName);
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Zone Group: Down\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Style: Up\n");
	Format(C_buffer, 512, "%s > Current Style: %s", C_buffer, gI_Styles[gI_PlayerReplay[I_Client][1]][StyleName]);
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Style: Down\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Play Replay\n");

	if (gI_TimerReplayRecords[gI_PlayerReplay[I_Client][0]][gI_PlayerReplay[I_Client][1]][0] != 0) {
		misc_FormatTime(gI_TimerReplayRecords[gI_PlayerReplay[I_Client][0]][gI_PlayerReplay[I_Client][1]][0], C_Time, 64);
		gA_Names.GetString(gI_TimerReplayRecords[gI_PlayerReplay[I_Client][0]][gI_PlayerReplay[I_Client][1]][1], C_PlayerName, 128);

		Format(C_buffer, 512, "%s   > Time: %s\n", C_buffer, C_Time);
		Format(C_buffer, 512, "%s   > Player: %s", C_buffer, C_PlayerName);
		M_Menu.AddItem(C_buffer, C_buffer);
	} else {
		Format(C_buffer, 512, "%s   > No Records", C_buffer);
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	}

	M_Menu.Display(I_Client, 0);
}

public int ReplayMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	bool B_DontShow;
	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				if (gI_PlayerReplay[I_Param1][0] == gI_TotalZoneGroups - 1) {
					gI_PlayerReplay[I_Param1][0] = 0;
				} else {
					gI_PlayerReplay[I_Param1][0]++;
				}
			} case 1: {
				if (gI_PlayerReplay[I_Param1][0] == 0) {
					gI_PlayerReplay[I_Param1][0] = gI_TotalZoneGroups - 1;
				} else {
					gI_PlayerReplay[I_Param1][0]--;
				}
			} case 2: {
				if (gI_PlayerReplay[I_Param1][1] == gI_TotalStyles - 1) {
					gI_PlayerReplay[I_Param1][1] = 0;
				} else {
					gI_PlayerReplay[I_Param1][1]++;
				}
			} case 3: {
				if (gI_PlayerReplay[I_Param1][1] == 0) {
					gI_PlayerReplay[I_Param1][1] = gI_TotalStyles - 1;
				} else {
					gI_PlayerReplay[I_Param1][1]--;
				}
			} case 4: {
				replay_TryQueueReplay(I_Param1, gI_PlayerReplay[I_Param1][0], gI_PlayerReplay[I_Param1][1]);
				B_DontShow = true;
			}
		}

		if (!B_DontShow) {
			misc_ReplayMenu(I_Param1);
		}
	}
}

public void misc_Exp(int I_Client, int I_ZoneGroup, int I_Style) {
	char C_buffer[512];
	float F_Total, F_Pow, F_MapModifier, F_Vip;
	int I_Total, I_NewLevel;

	F_Pow = Pow(1.02, float(gI_TimerPlayerAttempts[I_Client][gI_TimerCurrentSeason[I_Client]][I_ZoneGroup][I_Style]));
	F_MapModifier = (float(1) / float(gI_MapInfo[I_ZoneGroup][0]));

	F_Total = (float(1) + (float(1) / F_Pow));
	F_Total = (F_Total / F_MapModifier);
	F_Total *= gI_Styles[I_Style][ExpMultiplier];
	F_Total *= 1000.0;

	F_Vip = (F_Total * gF_VipExp[gI_TimerPlayerVip[I_Client][0]]);
	F_Total *= EXP_MULTIPLIER;

	F_Total += F_Vip;
	I_Total = RoundToFloor(F_Total);

	for (int i = 0; i < 2; i++) {
		gI_TimerPlayerExp[I_Client][gI_TimerCurrentSeason[I_Client]][i] += I_Total;
	}

	I_NewLevel = misc_CalculateLevel(gI_TimerPlayerExp[I_Client][gI_TimerCurrentSeason[I_Client]][0]);

	if (gI_TimerPlayerSettings[I_Client][3] == 1) {
		if (I_NewLevel > gI_TimerPlayerLevel[I_Client]) {
			Format(C_buffer, 512, " \x01Leveled \x06UP \x01to: \x10%i", I_NewLevel);
		}

		if (EXP_MULTIPLIER > 1.0) {
			Format(C_buffer, 512, " [%.1f* Multiplier]%s", EXP_MULTIPLIER, C_buffer);
		}

		if (gI_TimerPlayerVip[I_Client][0] > 0) {
			Format(C_buffer, 512, " [+%i%s Vip]%s", RoundToFloor(gF_VipExp[gI_TimerPlayerVip[I_Client][0]] * 100), PERCENT, C_buffer);
		}

		Format(C_buffer, 512, "%s Exp: \x06+%i\x01%s", PLUGIN_PREFIX, I_Total, C_buffer);
		PrintToChat(I_Client, C_buffer);
	}

	Format(C_buffer, 512, "UPDATE `t_exp` SET currentexp = currentexp + '%i', totalexp = totalexp + '%i' WHERE uid = '%i' AND season = '%i'", I_Total, I_Total, gI_TimerPlayerUid[I_Client], gI_TimerCurrentSeason[I_Client]);
	gA_SqlQueries.PushString(C_buffer);

	gI_TimerPlayerLevel[I_Client] = I_NewLevel;
}

public int misc_CalculateLevel(int I_Exp) {
	int I_Level = 1;
	float F_Boundry, F_Pow;

	while ((I_Exp > 0) && (I_Level <= 100)) {
		F_Pow = Pow(1.09, float(I_Level));

		F_Boundry = float(5000) + ((F_Pow) * 100.0);

		if (I_Exp >= F_Boundry) {
			I_Level++;
		}

		I_Exp -= RoundToFloor(F_Boundry);
	}

	return I_Level - 1;
}

public int misc_CalculateExp(int I_Level) {
	float F_Exp, F_Pow;

	for (int i = 1; i <= I_Level; i++) {
		F_Pow = Pow(1.09, float(i));

		F_Exp += float(5000) + ((F_Pow) * 100.0);
	}

	return RoundToFloor(F_Exp);
}

public void misc_ConVars() {
	ConVar bot_quota_mode = FindConVar("bot_quota_mode");
	bot_quota_mode.SetString("normal");

	ConVar bot_join_after_player = FindConVar("bot_join_after_player");
	bot_join_after_player.SetBool(false);

	ConVar mp_autoteambalance = FindConVar("mp_autoteambalance");
	mp_autoteambalance.SetBool(false);

	ConVar mp_limitteams = FindConVar("mp_limitteams");
	mp_limitteams.SetInt(0);

	ConVar bot_zombie = FindConVar("bot_zombie");
	bot_zombie.SetBool(true);

	ConVar sv_clamp_unsafe_velocities = FindConVar("sv_clamp_unsafe_velocities");
	sv_clamp_unsafe_velocities.SetBool(false);

	ConVar mp_maxrounds = FindConVar("mp_maxrounds");
	mp_maxrounds.SetInt(0);

	ConVar mp_timelimit = FindConVar("mp_timelimit");
	mp_timelimit.SetInt(30);

	ConVar mp_roundtime = FindConVar("mp_roundtime");
	mp_roundtime.SetInt(30);

	ConVar mp_freezetime = FindConVar("mp_freezetime");
	mp_freezetime.SetInt(0);

	ConVar mp_ignore_round_win_conditions = FindConVar("mp_ignore_round_win_conditions");
	mp_ignore_round_win_conditions.SetBool(false);

	ConVar mp_match_end_changelevel = FindConVar("mp_match_end_changelevel");
	mp_match_end_changelevel.SetBool(true);

	ConVar sv_accelerate_use_weapon_speed = FindConVar("sv_accelerate_use_weapon_speed");
	sv_accelerate_use_weapon_speed.SetBool(false);

	ConVar mp_free_armor = FindConVar("mp_free_armor");
	mp_free_armor.SetBool(true);

	ConVar sv_full_alltalk = FindConVar("sv_full_alltalk");
	sv_full_alltalk.SetInt(1);

	ConVar sv_alltalk = FindConVar("sv_alltalk");
	sv_alltalk.SetBool(true);

	ConVar sv_friction = FindConVar("sv_friction");
	sv_friction.SetInt(4);
}

public void misc_PreviousSeasonsMenu(int I_Client) {
	char C_buffer[512];
	Menu M_Menu = new Menu(PreviousSeasonsHandle);

	Format(C_buffer, 512, "%s - Seasons - Previous Seasons\n", MENU_PREFIX);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);
}

public int PreviousSeasonsHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
}

public misc_NewDayFire() {
	//Temp
}

public void misc_Precache() {
	gH_Models[LaserMaterial] = PrecacheModel("materials/sprites/laserbeam.vmt");
	gH_Models[HaloMaterial] = PrecacheModel("materials/sprites/glow01.vmt");
	gH_Models[GlowSprite] = PrecacheModel("materials/sprites/blueflare1.vmt");
	gH_Models[BlueGlowSprite] = PrecacheModel("sprites/blueglow1.vmt");
	gH_Models[Barrel] = PrecacheModel("models/props/de_train/barrel.mdl");
}

public void misc_GiveParticleItem(int I_Client, int I_Index, int I_Type) {
	char C_buffer[512];
	int I_Entity;
	float F_Pos[3];

	misc_TryKillItem(I_Client, I_Type);

	if (I_Index > -1) {
		if (I_Type != 2) {
			gA_Items[I_Type][1].GetString(I_Index, C_buffer, 512);
		} else {
			Format(C_buffer, 512, "halo_final");
		}

		if (IsPlayerAlive(I_Client)) {
			I_Entity = CreateEntityByName("info_particle_system");

			if (IsValidEdict(I_Entity) && I_Entity > 0) {
				GetClientAbsOrigin(I_Client, F_Pos);
				TeleportEntity(I_Entity, F_Pos, NULL_VECTOR, NULL_VECTOR);

				DispatchKeyValue(I_Entity, "effect_name", C_buffer);
				SetVariantString("!activator");
				AcceptEntityInput(I_Entity, "SetParent", I_Client, I_Entity, 0);
				DispatchSpawn(I_Entity);
				ActivateEntity(I_Entity);

				AcceptEntityInput(I_Entity, "Start");
				SetEntPropEnt(I_Entity, Prop_Send, "m_hOwnerEntity", I_Client);

				gI_TimerPlayerItems[I_Client][I_Type] = I_Entity;
				SDKHook(I_Entity, SDKHook_SetTransmit, Hook_EntityOnTransmit);
			}
		}
	}
}

public void misc_TryKillItem(int I_Client, int I_Type) {
	if (gI_TimerPlayerItems[I_Client][I_Type] != 0) {
		if (IsValidEntity(gI_TimerPlayerItems[I_Client][I_Type])) {
			AcceptEntityInput(gI_TimerPlayerItems[I_Client][I_Type], "Kill");
			SDKUnhook(gI_TimerPlayerItems[I_Client][I_Type], SDKHook_SetTransmit, Hook_EntityOnTransmit);
		}
	}

	gI_TimerPlayerItems[I_Client][I_Type] = 0;
}

public void misc_ItemsStarted(int I_Client) {
	for (int i = 0; i < 2; i++) {
		if (strlen(gC_TimerPlayerPrestigeShopValues[I_Client][i + 3]) > 0) {
			int I_Item = StringToInt(gC_TimerPlayerPrestigeShopValues[I_Client][i + 3]);

			misc_GiveParticleItem(I_Client, I_Item, i);
		}
	}
}

public void misc_ClearLocalSettings(int I_Client) {
	for (int i = 0; i < 64; i++) {
		gI_TimerMiscMultipleSettings[I_Client][i] = -1;
	}
}
