public Action Hook_OnTakeDamageCallback(int victim, int &attacker, int &inflictor, float &damage, int &damagetype) {
	damage *= 0;
	return Plugin_Handled;
}

public Action Hook_OnWeaponDropPostCallback(int I_Client, int I_Weapon) {
	CreateTimer(1.0, Timer_ClearEntity, I_Weapon);
}

public Action Hook_EntityOnTransmit(int I_Entity, int I_Client) {
	int I_Owner = GetEntPropEnt(I_Entity, Prop_Send, "m_hOwnerEntity");

	if ((I_Owner != I_Client) && IsValidClient(I_Client) && (gI_TimerPlayerVip[I_Owner][0] < 3)) {
		if (gB_TimerPlayerHide[I_Client] && IsPlayerAlive(I_Client)) {
			return Plugin_Handled;
		}
	}

	return Plugin_Continue;
}

public Action Hook_OnTransmit(int I_Entity, int I_Client) {
	if ((I_Entity != I_Client) && (IsFakeClient(I_Entity) || IsValidClient(I_Client)) && (gI_TimerPlayerVip[I_Entity][0] < 2)) {
		if (gB_TimerPlayerHide[I_Client]) {
			if (IsPlayerAlive(I_Client)) {
				return Plugin_Handled;
			} else {
				if (IsClientObserver(I_Client)) {
					int I_ClientSpecMode = GetEntProp(I_Client, Prop_Send, "m_iObserverMode");

					if (I_ClientSpecMode == 4 || I_ClientSpecMode == 5) {
						int I_Target = GetEntPropEnt(I_Client, Prop_Send, "m_hObserverTarget");
						if (I_Target != I_Entity) {
							return Plugin_Handled;
						}
					}
				}
			}
		}

		if (gB_TimerPlayerHide[I_Client] && IsPlayerAlive(I_Client)) {
			return Plugin_Handled;
		}
	}

	return Plugin_Continue;
}

public Action Hook_WeaponCanUse(int I_Client, int I_Weapon) {
	if (gB_TimerPlayerDisablePickup[I_Client]) {
		if (GetEntProp(I_Weapon, Prop_Send, "m_hPrevOwner") != 0) {
			return Plugin_Handled;
		}
	}

	return Plugin_Continue;
}

public Action HookEvent_RoundStartPre(Event E_Event, char[] C_Name, bool B_DontBroadcast) {
	bool B_RealClients;

	for (int i = 1; i <= MaxClients; i++) {
		if (IsValidClient(i)) {
			B_RealClients = true;
			break;
		}
	}

	if (!B_RealClients) {
		return Plugin_Handled;
	}

	return Plugin_Continue;
}

public Action HookEvent_RoundStartPost(Event E_Event, char[] C_Name, bool B_DontBroadcast) {
	zone_EntityReload();

	CreateTimer(0.5, Timer_ResetAutoBot);

	for (int i = 0; i <= MaxClients; i++) {
		if (IsValidClient(i)) {
			misc_DelayedTeleportClientToZone(i, 0, 0, 1);
			misc_ItemsStarted(i);
		}
	}

	replay_SpawnBots();

	/*
	misc_HookTriggers("trigger_multiple", "OnTrigger");
	misc_HookTriggers("trigger_multiple", "OnStartTouching");
	misc_HookTriggers("trigger_multiple", "OnTouching");
	misc_HookTriggers("trigger_multiple", "OnEndTouch");

	misc_HookSingleTriggers("trigger_multiple", "OnTrigger");
	misc_HookSingleTriggers("trigger_multiple", "OnStartTouching");
	misc_HookSingleTriggers("trigger_multiple", "OnTouching");
	misc_HookSingleTriggers("trigger_multiple", "OnEndTouch");
	*/
}

public Action HookEvent_RoundEnd(Event E_Event, char[] C_Name, bool B_DontBroadcast) {
	for (int i = 0; i <= MaxClients; i++) {
		if (IsValidClient(i)) {
			for (int x = 0; x < 3; x++) {
				 misc_TryKillItem(i, x);
			}
		}
	}
}

public Action HookEvent_GameEnd(Event E_Event, char[] C_Name, bool B_DontBroadcast) {
	if (gB_ChangingMaps) {
		return Plugin_Continue;
	}

	return Plugin_Handled;
}

public Action HookEvent_PlayerDeath(Event E_Event, char[] C_Name, bool B_DontBroadcast) {
	int I_Client = GetClientOfUserId(E_Event.GetInt("userid"));

	if (IsValidEntity(I_Client)) {
		for (int i = 0; i < 3; i++) {
			misc_TryKillItem(I_Client, i);
		}
	}
}

public Action HookEvent_PlayerSpawn(Event E_Event, char[] C_Name, bool B_DontBroadcast) {
	int I_Client = GetClientOfUserId(E_Event.GetInt("userid"));

	if (IsValidEntity(I_Client)) {
		SetEntProp(I_Client, Prop_Data, "m_CollisionGroup", COLLISION_GROUP_DEBRIS_TRIGGER);

		misc_ItemsStarted(I_Client);

		if (gB_TimerPlayerNew[I_Client]) {
			CreateTimer(0.5, Timer_WelcomeMessage, GetClientUserId(I_Client));
		}
	}
}

public Action HookEvent_PlayerTeam(Event E_Event, char[] C_Name, bool B_DontBroadcast) {
	int I_Client = GetClientOfUserId(E_Event.GetInt("userid"));
	int I_Team = GetClientOfUserId(E_Event.GetInt("team"));

	B_DontBroadcast = true;
	E_Event.BroadcastDisabled = true;

	if (IsValidClient(I_Client)) {
		for (int i = 0; i < 3; i++) {
			misc_TryKillItem(I_Client, i);
		}

		if (I_Team > 2) {
			return Plugin_Handled;
		}
	}

	return Plugin_Changed
}

public Action HookEvent_PlayerJump(Event E_Event, char[] C_Name, bool B_DontBroadcast) {
	char C_buffer[512];
	int I_Client, I_Modulo, I_Diff, I_DiffSync[2];
	float F_Total, F_Speed, F_PreviousSpeed, F_DiffSpeed, F_DiffSync, F_Velocity[3];

	I_Client = GetClientOfUserId(E_Event.GetInt("userid"));

	GetEntPropVector(I_Client, Prop_Data, "m_vecVelocity", F_Velocity);
	F_Speed = SquareRoot(Pow(F_Velocity[0], 2.0) + Pow(F_Velocity[1], 2.0));

	if (IsValidClient(I_Client)) {
		gI_TimerCurrentJumps[I_Client]++;

		if (gI_TimerCurrentJumpInterval[I_Client] != 0) {
			if (gI_TimerCurrentJumps[I_Client] == 1) {
				Format(C_buffer, 512, "%s Prestrafe Speed: \x10%.2f", PLUGIN_PREFIX, F_Speed);
				misc_PrintToChat(I_Client, C_buffer, false, 0, true, true, true);
			}

			I_Modulo = gI_TimerCurrentJumps[I_Client] % gI_TimerCurrentJumpInterval[I_Client];

			gA_TimerJumpStats[I_Client].Resize(gI_TimerCurrentJumps[I_Client] + 1);

			gA_TimerJumpStats[I_Client].Set(gI_TimerCurrentJumps[I_Client], F_Speed, 0);
			gA_TimerJumpStats[I_Client].Set(gI_TimerCurrentJumps[I_Client], gI_TimerCurrentSync[I_Client][0], 1);
			gA_TimerJumpStats[I_Client].Set(gI_TimerCurrentJumps[I_Client], gI_TimerCurrentSync[I_Client][1], 2);

			if (gI_TimerCurrentJumps[I_Client] > 1) {
				if (gB_TimerCurrentJumpSettings[I_Client][0]) {
					if (I_Modulo == 0) {
						if (gI_TimerCurrentJumps[I_Client] - gI_TimerCurrentJumpInterval[I_Client] > 0) {
							I_Diff = gI_TimerCurrentJumps[I_Client] - gI_TimerCurrentJumpInterval[I_Client];
						} else {
							I_Diff = 1;
						}

						F_PreviousSpeed = gA_TimerJumpStats[I_Client].Get(I_Diff, 0);
						F_DiffSpeed = F_Speed - F_PreviousSpeed;

						for (int i = 0; i < 2; i++) {
							I_DiffSync[i] = gA_TimerJumpStats[I_Client].Get(gI_TimerCurrentJumps[I_Client], i + 1) - gA_TimerJumpStats[I_Client].Get(I_Diff, i + 1);
						}

						F_DiffSync = ((float(I_DiffSync[0]) + 1) / (float(I_DiffSync[1]) + 1) * 100.0);

						Format(C_buffer, 512, "%s J: \x07%i \x01S: \x10%.2f \x01%s%.2f \x01Sy: %.2f", PLUGIN_PREFIX, gI_TimerCurrentJumps[I_Client], F_Speed, (F_DiffSpeed > 0.0 ? "SpG: \x06+" : "SpG \x07"), F_DiffSpeed, F_DiffSync);
						misc_PrintToChat(I_Client, C_buffer, false, 0, true, true, true);
					}
				} else {
					F_Total = float(gI_TimerCurrentJumps[I_Client]) / float(gI_TimerCurrentJumpInterval[I_Client]);

					if (F_Total == 1.0) {
						F_DiffSync = ((float(gI_TimerCurrentSync[I_Client][0]) + 1) / (float(gI_TimerCurrentSync[I_Client][1]) + 1) * 100.0);

						Format(C_buffer, 512, "%s J: \x07%i \x01S: \x10%.2f \x01Sy: %.2f", PLUGIN_PREFIX, gI_TimerCurrentJumps[I_Client], F_Speed, F_DiffSync);
						misc_PrintToChat(I_Client, C_buffer, false, 0, true, true, true);
					}
				}
			}
		}
	}
}

public Action HookEvent_PlayerDisconnect(Event E_Event, char[] C_Name, bool B_DontBroadcast) {
	B_DontBroadcast = true;
	E_Event.BroadcastDisabled = true;

	return Plugin_Handled;
}

public Action HookEvent_PlayerConnect(Event E_Event, char[] C_Name, bool B_DontBroadcast) {
	B_DontBroadcast = true;
	E_Event.BroadcastDisabled = true;

	return Plugin_Handled;
}

public Action HookEvent_RoundFreezeEnd(Event E_Event, char[] C_Name, bool B_DontBroadcast) {
	ConVar sv_full_alltalk = FindConVar("sv_full_alltalk");
	sv_full_alltalk.SetInt(1);
}

public Action UserMsg_TextMsg(UserMsg msg_id, BfRead msg, const int[] players, int playersNum, bool reliable, bool init) {
	char C_buffer[512];
	PbReadString(msg, "params", C_buffer, sizeof(C_buffer), 0);

	if (StrEqual(C_buffer, "#Player_Cash_Award_ExplainSuicide_YouGotCash")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_ExplainSuicide_Spectators")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_ExplainSuicide_EnemyGotCash")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_ExplainSuicide_TeammateGotCash")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#game_respawn_as")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#game_spawn_as")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_Killed_Enemy")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Team_Cash_Award_Win_Time")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Point_Award_Assist_Enemy_Plural")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Point_Award_Assist_Enemy")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Point_Award_Killed_Enemy_Plural")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Point_Award_Killed_Enemy")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_Respawn")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_Get_Killed")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_Killed_Enemy")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_Killed_Enemy_Generic")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_Kill_Teammate")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Team_Cash_Award_Loser_Bonus")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Team_Cash_Award_Loser_Zero")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Team_Cash_Award_no_income")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Team_Cash_Award_Generic")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Team_Cash_Award_Custom")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_ExplainSuicide_YouGotCash")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_ExplainSuicide_TeammateGotCash")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_ExplainSuicide_EnemyGotCash")) {
		return Plugin_Handled;
	} else if (StrEqual(C_buffer, "#Player_Cash_Award_ExplainSuicide_Spectators")) {
		return Plugin_Handled;
	}

	return Plugin_Continue;
}

public Action UserMsg_SayText2(UserMsg msg_id, BfRead msg, const int[] players, int playersNum, bool reliable, bool init) {
	char C_buffer[512];

	if (GetUserMessageType() == 1) {
		PbReadString(msg, "msg_name", C_buffer, sizeof(C_buffer), -1);

		if (StrEqual(C_buffer, "#Cstrike_Name_Change")) {
			return Plugin_Handled;
		}
	}

	return Plugin_Continue;
}

public Action HookSound_ShotgunShot(const char[] te_name, const int[] Players, int numClients, float delay) {
	int[] I_NewClients = new int[MaxClients + 1];
	int I_NewTotal;
	float vTemp[3];

	for (int i = 1; i <= MaxClients; i++) {
		if (IsValidClient(i)) {
			if (gI_TimerPlayerSettings[i][0] == 1) {
				I_NewClients[I_NewTotal] = i;
				I_NewTotal++;
			}
		}
	}

	if (I_NewTotal == numClients) {
		return Plugin_Continue;
	} else if (I_NewTotal == 0) {
		return Plugin_Stop;
	}

	TE_Start("Shotgun Shot");
	TE_ReadVector("m_vecOrigin", vTemp);
	TE_WriteVector("m_vecOrigin", vTemp);
	TE_WriteFloat("m_vecAngles[0]", TE_ReadFloat("m_vecAngles[0]"));
	TE_WriteFloat("m_vecAngles[1]", TE_ReadFloat("m_vecAngles[1]"));
	TE_WriteNum("m_iWeaponID", TE_ReadNum("m_iWeaponID"));
	TE_WriteNum("m_iMode", TE_ReadNum("m_iMode"));
	TE_WriteNum("m_iSeed", TE_ReadNum("m_iSeed"));
	TE_WriteNum("m_iPlayer", TE_ReadNum("m_iPlayer"));
	TE_WriteFloat("m_fInaccuracy", TE_ReadFloat("m_fInaccuracy"));
	TE_WriteFloat("m_fSpread", TE_ReadFloat("m_fSpread"));
	TE_Send(I_NewClients, I_NewTotal, delay);

	return Plugin_Stop;
}

public Action HookEvent_Chat(int I_Client, char[] C_Command, int I_Argc) {
	char C_buffer[512];

	if (IsChatTrigger()) {
		return Plugin_Handled;
	}

	GetCmdArgString(C_buffer, 512);
	StripQuotes(C_buffer);

	if ((C_buffer[0] == '!') || (C_buffer[0] == '/')) {
		for (int i = 0; i < strlen(C_buffer); i++) {
			C_buffer[i] = CharToLower(C_buffer[i + 1]);
		}

		FakeClientCommand(I_Client, "sm_%s", C_buffer);
		return Plugin_Handled;
	}

	return Plugin_Continue;
}
