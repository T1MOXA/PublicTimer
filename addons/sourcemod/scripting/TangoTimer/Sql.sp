public void sql_PluginStart() {
	char C_Error[3][512];

	gD_Main = SQL_Connect("TangoTimerMain", true, C_Error[0], 512);
	gD_Slave = SQL_Connect("TangoTimerSlave", true, C_Error[1], 512);
	gD_Log = SQL_Connect("TangoTimerLog", true, C_Error[2], 512);

	if (gD_Main == INVALID_HANDLE) {
		CloseHandle(gD_Main);
		SetFailState("%s - (Connect [Main]) - %s", SQL_PREFIX, C_Error[0]);
	}

	if (gD_Slave == INVALID_HANDLE) {
		CloseHandle(gD_Slave);
		SetFailState("%s - (Connect [Slave]) - %s", SQL_PREFIX, C_Error[1]);
	}

	if (gD_Log == INVALID_HANDLE) {
		CloseHandle(gD_Log);
		SetFailState("%s - (Connect [Log]) - %s", SQL_PREFIX, C_Error[2]);
	}

	sql_CheckTables();
}

public void sql_CheckTables() {
	bool B_Failed;

	SQL_LockDatabase(gD_Main);

	if (!SQL_FastQuery(gD_Main, "SELECT * FROM `t_players` LIMIT 1")) {
		B_Failed = true;
	}

	SQL_UnlockDatabase(gD_Main);

	if (B_Failed) {
		sql_CreateTables();
	} else {
		sql_LogStart();
		sql_LoadTables();
	}
}

public void sql_CreateTables() {
	char C_buffer[512];

	Transaction T_Main = SQL_CreateTransaction();
	Transaction T_Log = SQL_CreateTransaction();

	/* MAIN */
	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_attempts` (mapname VARCHAR(128) NOT NULL, uid INT NOT NULL, style INT NOT NULL, zonegroup INT NOT NULL, season INT NOT NULL, value INT NOT NULL, PRIMARY KEY (`mapname`, `uid`, `style`, `zonegroup`, `season`))");
	T_Main.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_cache` (uid INT NOT NULL, style INT NOT NULL, value FLOAT NOT NULL, PRIMARY KEY (`uid`, `style`));");
	T_Main.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_checkpoints` (mapname VARCHAR(128) NOT NULL, uid INT NOT NULL, checkpointid INT NOT NULL, checkpointtime INT NOT NULL, style INT NOT NULL, zonegroup INT NOT NULL, PRIMARY KEY (`mapname`, `uid`, `checkpointid`, `style`, `zonegroup`));");
	T_Main.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_exp` (uid INT NOT NULL, currentexp INT(16) NOT NULL, totalexp INT(16) NOT NULL, currentprestige INT NOT NULL, season INT NOT NULL, PRIMARY KEY (`uid`, `season`));");
	T_Main.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_mapinfo` (mapname VARCHAR(128) NOT NULL, zonegroup INT NOT NULL, maptier INT NOT NULL, mapstyle INT NOT NULL, PRIMARY KEY (`mapname`, `zonegroup`));");
	T_Main.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_players` (uid INT NOT NULL AUTO_INCREMENT, steamid VARCHAR(64) NOT NULL, lastname VARCHAR(128) NOT NULL, lastserver VARCHAR(8) NOT NULL, firstconnect INT(16) NOT NULL, lastconnect INT(16) NOT NULL, prestigetokens INT NOT NULL, season INT NOT NULL, vip INT(3) NOT NULL, vipend INT NOT NULL, PRIMARY KEY (`uid`));");
	T_Main.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_records` (mapname VARCHAR(128) NOT NULL, uid INT NOT NULL, time INT(8) NOT NULL, sync FLOAT NOT NULL, jumps INT NOT NULL, style INT NOT NULL, zonegroup INT NOT NULL, server VARCHAR(18) NOT NULL, timestamp INT(16) NOT NULL, PRIMARY KEY (`mapname`, `uid`, `style`, `zonegroup`));");
	T_Main.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_settings` (uid INT NOT NULL, setting INT NOT NULL, value INT NOT NULL, PRIMARY KEY (`uid`, `setting`));");
	T_Main.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_zones` (mapname VARCHAR(128) NOT NULL, zoneid INT NOT NULL AUTO_INCREMENT, zonetype INT NOT NULL, zonegroup INT NOT NULL, a_x FLOAT NOT NULL, a_y FLOAT NOT NULL, a_z FLOAT NOT NULL, b_x FLOAT NOT NULL, b_y FLOAT NOT NULL, b_z FLOAT NOT NULL, PRIMARY KEY (`zoneid`));");
	T_Main.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_race` (uid INT NOT NULL, season INT NOT NULL, wins INT NOT NULL, loss INT NOT NULL, elo INT NOT NULL, PRIMARY KEY (`uid`, `season`));");
	T_Main.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_prestigeshop` (uid INT NOT NULL, item INT NOT NULL, value VARCHAR(512) NOT NULL, PRIMARY KEY (`uid`, `item`));");
	T_Main.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_titles` (uid INT NOT NULL, title INT NOT NULL, value INT NOT NULL, PRIMARY KEY (`uid`, `title`));");
	T_Main.AddQuery(C_buffer);

	/* Log */
	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_server` (serverip VARCHAR(32) NOT NULL, type VARCHAR(32) NOT NULL, timestamp INT(16) NOT NULL, data VARCHAR(512) NOT NULL, PRIMARY KEY (`type`, `timestamp`));");
	T_Log.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_playeractivity` (serverip VARCHAR(32), steamid VARCHAR(64) NOT NULL, playerip VARCHAR(32) NOT NULL, mapname VARCHAR(128) NOT NULL, type VARCHAR(32) NOT NULL, timestamp INT(16) NOT NULL, length INT NOT NULL, PRIMARY KEY (`steamid`, `timestamp`));");
	T_Log.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `t_finish` (serverip VARCHAR(32), steamid VARCHAR(64) NOT NULL, playerip VARCHAR(32) NOT NULL, mapname VARCHAR(128) NOT NULL, style INT NOT NULL, zonegroup INT NOT NULL, exp INT(4) NOT NULL, timestamp INT(16) NOT NULL, PRIMARY KEY (`steamid`, `mapname`, `style`, `zonegroup`, `timestamp`));");
	T_Log.AddQuery(C_buffer);

	SQL_ExecuteTransaction(gD_Main, T_Main, sqlMainCreateTablesSuccess, sqlMainCreateTablesError, _, DBPrio_High);
	SQL_ExecuteTransaction(gD_Log, T_Log, sqlMainCreateTablesSuccess, sqlMainCreateTablesError, _, DBPrio_High);
}

public void sql_LogStart() {
	char C_buffer[512];
	int I_TimeStamp;

	I_TimeStamp = GetTime();

	Format(C_buffer, 512, "INSERT INTO `t_server` VALUES ('%s', 'Plugin Start', '%i', '')", gC_ServerIp, I_TimeStamp);
	gA_SqlQueries.PushString(C_buffer);
}

public void sql_LogEnd() {
	char C_buffer[512];
	int I_TimeStamp;

	I_TimeStamp = GetTime();

	Format(C_buffer, 512, "INSERT INTO `t_server` VALUES ('%s', 'Plugin End', '%i', '')", gC_ServerIp, I_TimeStamp);
	gA_SqlQueries.PushString(C_buffer);
}

public void sql_LogMapStart() {
	char C_buffer[512];
	int I_TimeStamp;

	I_TimeStamp = GetTime();

	Format(C_buffer, 512, "INSERT INTO `t_server` VALUES ('%s', 'Map Start', '%i', 'Map: %s')", gC_ServerIp, I_TimeStamp, gC_CurrentMap);
	gA_SqlQueries.PushString(C_buffer);
}

public void sql_LogMapEnd() {
	char C_buffer[512];
	int I_TimeStamp;
	float F_GameTime;

	I_TimeStamp = GetTime();
	F_GameTime = GetGameTime();

	Format(C_buffer, 512, "INSERT INTO `t_server` VALUES ('%s', 'Map End', '%i', 'Map: %s Time: %f')", gC_ServerIp, I_TimeStamp, gC_CurrentMap, F_GameTime);
	gA_SqlQueries.PushString(C_buffer);
}

public void sql_LogPlayerConnect(int I_Client) {
	char C_buffer[512];
	int I_TimeStamp;

	I_TimeStamp = GetTime();
	F_JoinTimeStamp[I_Client] = GetGameTime();

	Format(C_buffer, 512, "INSERT INTO `t_playeractivity` VALUES ('%s', '%s', '%s', '%s', 'Connect', '%i', '0')", gC_ServerIp, gC_TimerPlayerSteamId[I_Client], gC_TimerPlayerIp[I_Client], gC_CurrentMap, I_TimeStamp);
	gA_SqlQueries.PushString(C_buffer);
}

public void sql_LogPlayerDisconnect(int I_Client) {
	char C_buffer[512];
	int I_TimeStamp, I_ConnectTime;

	I_TimeStamp = GetTime();

	I_ConnectTime = RoundToFloor(GetGameTime() - F_JoinTimeStamp[I_Client]);
	F_JoinTimeStamp[I_Client] = 0.0;

	Format(C_buffer, 512, "INSERT INTO `t_playeractivity` VALUES ('%s', '%s', '%s', '%s', 'Disconnect', '%i', '%i')", gC_ServerIp, gC_TimerPlayerSteamId[I_Client], gC_TimerPlayerIp[I_Client], gC_CurrentMap, I_TimeStamp, I_ConnectTime);
	gA_SqlQueries.PushString(C_buffer);
}

public void sql_LoadMapInfo() {
	char C_buffer[512];

	Format(C_buffer, 512, "SELECT * FROM `t_mapinfo` WHERE mapname = '%s' ORDER BY `zonegroup` ASC", gC_CurrentMap);
	SQL_TQuery(gD_Slave, sqlLoadMapInfoCallback, C_buffer, _, DBPrio_Normal);
}

public void sql_LoadZones() {
	char C_buffer[512];

	Format(C_buffer, 512, "SELECT * FROM `t_zones` WHERE mapname = '%s' ORDER BY `zoneid` ASC", gC_CurrentMap);
	SQL_TQuery(gD_Slave, sqlLoadZonesCallback, C_buffer, _, DBPrio_Normal);
}

public void sql_CheckZone(int I_Client, int I_ZoneType, int I_ZoneGroup, float F_PointA[3], float F_PointB[3]) {
	char C_buffer[512];
	int I_UserId;

	DataPack D_Pack = new DataPack();
	I_UserId = GetClientUserId(I_Client);

	D_Pack.WriteCell(I_UserId);
	D_Pack.WriteCell(I_ZoneType);
	D_Pack.WriteCell(I_ZoneGroup);

	for (int i = 0; i < 3; i++) {
		D_Pack.WriteFloat(F_PointA[i]);
	}

	for (int i = 0; i < 3; i++) {
		D_Pack.WriteFloat(F_PointB[i]);
	}

	Format(C_buffer, 512, "SELECT * FROM `t_zones` WHERE mapname = '%s' AND zonetype = '%i' AND zonegroup = '%i'", gC_CurrentMap, I_ZoneType, I_ZoneGroup);
	SQL_TQuery(gD_Slave, sqlCheckZoneCallback, C_buffer, D_Pack, DBPrio_High);
}

public void sql_InsertZone(int I_Client, int I_ZoneType, int I_ZoneGroup, float F_PointA[3], float F_PointB[3]) {
	char C_buffer[512];

	Format(C_buffer, 512, "INSERT INTO `t_zones` VALUES ('%s', '0', '%i', '%i', '%.3f', '%.3f', '%.3f', '%.3f', '%.3f', '%.3f')", gC_CurrentMap, I_ZoneType, I_ZoneGroup, F_PointA[0], F_PointA[1], F_PointA[2], F_PointB[0], F_PointB[1], F_PointB[2]);
	SQL_TQuery(gD_Main, sqlInsertZoneCallback, C_buffer, GetClientUserId(I_Client), DBPrio_High);
}

public void sql_UpdateZone(int I_Client, int I_ZoneId, float F_PointA[3], float F_PointB[3]) {
	char C_buffer[512];

	Format(C_buffer, 512, "UPDATE `t_zones` SET a_x = '%.3f', a_y = '%.3f', a_z = '%.3f', b_x = '%.3f', b_y = '%.3f', b_z = '%.3f' WHERE zoneid = '%i'", F_PointA[0], F_PointA[1], F_PointA[2], F_PointB[0], F_PointB[1], F_PointB[2], I_ZoneId);
	SQL_TQuery(gD_Main, sqlUpdateZoneCallback, C_buffer, GetClientUserId(I_Client), DBPrio_High);
}

public void sql_LoadTables() {
	char C_buffer[512];

	Format(C_buffer, 512, "SELECT * FROM `t_players` ORDER BY uid ASC");
	SQL_TQuery(gD_Slave, sqlLoadTablesCallback, C_buffer, _, DBPrio_High);
}

public void sql_PlayerDisconnect(int I_Client) {
	char C_buffer[512], C_LastName[128], C_FormattedName[256];
	int I_TimeStamp;

	GetClientName(I_Client, C_LastName, 128);
	SQL_EscapeString(gD_Slave, C_LastName, C_FormattedName, 256);
	I_TimeStamp = GetTime();

	Format(C_buffer, 512, "UPDATE `t_players` SET lastname = '%s', lastconnect = '%i' WHERE uid = '%i'", C_FormattedName, I_TimeStamp, gI_TimerPlayerUid[I_Client]);
	gA_SqlQueries.PushString(C_buffer);
}

public void sql_LoadPlayerRecords(int I_Client) {
	char C_buffer[512];

	Format(C_buffer, 512, "SELECT * FROM `t_records` WHERE mapname = '%s' AND uid = '%i'", gC_CurrentMap, gI_TimerPlayerUid[I_Client]);
	SQL_TQuery(gD_Slave, sqlLoadPlayerRecordsCallback, C_buffer, GetClientUserId(I_Client), DBPrio_High);
}

public void sql_LoadMapRecords() {
	char C_buffer[512];

	Format(C_buffer, 512, "SELECT * FROM `t_records` WHERE mapname = '%s' ORDER BY `time` ASC", gC_CurrentMap);
	SQL_TQuery(gD_Slave, sqlLoadMapRecordsCallback, C_buffer, _, DBPrio_Normal);
}

public void sql_InsertRecord(int I_Client, int I_Time, float F_Sync, int I_Jumps, int I_Style, int I_ZoneGroup, int I_TimeStamp, ArrayList A_CheckpointData) {
	char C_buffer[512];

	Format(C_buffer, 512, "INSERT INTO `t_records` VALUES ('%s', '%i', '%i', '%.3f', '%i', '%i', '%i', 'US', '%i')", gC_CurrentMap, gI_TimerPlayerUid[I_Client], I_Time, F_Sync, I_Jumps, I_Style, I_ZoneGroup, I_TimeStamp);
	gA_SqlQueries.PushString(C_buffer);

	for (int i = 0; i < A_CheckpointData.Length; i++) {
		Format(C_buffer, 512, "INSERT INTO `t_checkpoints` VALUES ('%s', '%i', '%i', '%i', '%i', '%i')", gC_CurrentMap, gI_TimerPlayerUid[I_Client], i, A_CheckpointData.Get(i), I_Style, I_ZoneGroup);
		gA_SqlQueries.PushString(C_buffer);
	}

	delete A_CheckpointData;
}

public void sql_UpdateRecord(int I_Client, int I_Time, float F_Sync, int I_Jumps, int I_Style, int I_ZoneGroup, int I_TimeStamp, ArrayList A_CheckpointData) {
	char C_buffer[512];

	Format(C_buffer, 512, "UPDATE `t_records` SET time = '%i', sync = '%.3f', jumps = '%i', server = 'US', timestamp = '%i' WHERE mapname = '%s' AND uid = '%i' AND style = '%i' AND zonegroup = '%i'", I_Time, F_Sync, I_Jumps, I_TimeStamp, gC_CurrentMap, gI_TimerPlayerUid[I_Client], I_Style, I_ZoneGroup);
	gA_SqlQueries.PushString(C_buffer);

	for (int i = 0; i < A_CheckpointData.Length; i++) {
		Format(C_buffer, 512, "UPDATE `t_checkpoints` SET checkpointtime = '%i' WHERE mapname = '%s' AND uid = '%i' AND checkpointid = '%i' AND style = '%i' AND zonegroup = '%i'", A_CheckpointData.Get(i), gC_CurrentMap, gI_TimerPlayerUid[I_Client], i, I_Style, I_ZoneGroup);
		gA_SqlQueries.PushString(C_buffer);
	}

	delete A_CheckpointData;
}

public void sql_LoadPlayerData(int I_Client) {
	char C_buffer[512];
	Transaction T_Trans = SQL_CreateTransaction();

	Format(C_buffer, 512, "SELECT * FROM `t_players` WHERE uid = '%i'", gI_TimerPlayerUid[I_Client]);
	T_Trans.AddQuery(C_buffer);

	Format(C_buffer, 512, "SELECT * FROM `t_attempts` WHERE mapname = '%s' AND uid = '%i'", gC_CurrentMap, gI_TimerPlayerUid[I_Client]);
	T_Trans.AddQuery(C_buffer);

	Format(C_buffer, 512, "SELECT * FROM `t_exp` WHERE uid = '%i'", gI_TimerPlayerUid[I_Client]);
	T_Trans.AddQuery(C_buffer);

	Format(C_buffer, 512, "SELECT * FROM `t_settings` WHERE uid = '%i'", gI_TimerPlayerUid[I_Client]);
	T_Trans.AddQuery(C_buffer);

	Format(C_buffer, 512, "SELECT * FROM `t_titles` WHERE uid = '%i'", gI_TimerPlayerUid[I_Client]);
	T_Trans.AddQuery(C_buffer);

	Format(C_buffer, 512, "SELECT * FROM `t_cache` WHERE uid = '%i'", gI_TimerPlayerUid[I_Client]);
	T_Trans.AddQuery(C_buffer);

	Format(C_buffer, 512, "SELECT * FROM `t_prestigeshop` WHERE uid = '%i'", gI_TimerPlayerUid[I_Client]);
	T_Trans.AddQuery(C_buffer);

	Format(C_buffer, 512, "SELECT * FROM `t_race` WHERE uid = '%i' AND season = '%i'", gI_TimerPlayerUid[I_Client], SEASON);
	T_Trans.AddQuery(C_buffer);

	SQL_ExecuteTransaction(gD_Slave, T_Trans, sqlLoadPlayerDataSuccess, sqlLoadPlayerDataError, GetClientUserId(I_Client), DBPrio_High);
}

public void sql_InsertAttempts(int I_Client, int I_Style, int I_ZoneGroup) {
	char C_buffer[512];

	Format(C_buffer, 512, "INSERT INTO `t_attempts` VALUES ('%s', '%i', '%i', '%i', '%i', '1')", gC_CurrentMap, gI_TimerPlayerUid[I_Client], I_Style, I_ZoneGroup, gI_TimerCurrentSeason[I_Client]);
	gA_SqlQueries.PushString(C_buffer);
}

public void sql_UpdateAttempts(int I_Client, int I_Style, int I_ZoneGroup) {
	char C_buffer[512];

	Format(C_buffer, 512, "UPDATE `t_attempts` SET value = value + 1 WHERE mapname = '%s' AND uid = '%i' AND style = '%i' AND zonegroup = '%i' AND season = '%i'", gC_CurrentMap, gI_TimerPlayerUid[I_Client], I_Style, I_ZoneGroup, gI_TimerCurrentSeason[I_Client]);
	gA_SqlQueries.PushString(C_buffer);
}

public void sql_CheckMapInfo(int I_Client, int I_ZoneGroup, int I_Linear, int I_Tier) {
	char C_buffer[512];
	DataPack D_Pack = new DataPack();

	D_Pack.WriteCell(GetClientUserId(I_Client));
	D_Pack.WriteCell(I_ZoneGroup);
	D_Pack.WriteCell(I_Linear);
	D_Pack.WriteCell(I_Tier);

	Format(C_buffer, 512, "SELECT * FROM `t_mapinfo` WHERE mapname = '%s' AND zonegroup = '%i'", gC_CurrentMap, I_ZoneGroup);
	SQL_TQuery(gD_Slave, sqlCheckMapInfoCallback, C_buffer, D_Pack, DBPrio_Normal);
}

public void sql_TryLoadPlayerUid(int I_Client) {
	char C_buffer[512];

	Format(C_buffer, 512, "SELECT * FROM `t_players` WHERE steamid = '%s'", gC_TimerPlayerSteamId[I_Client], gC_TimerPlayerIp[I_Client]);
	SQL_TQuery(gD_Slave, sqlTryLoadPlayerUidCallback, C_buffer, GetClientUserId(I_Client), DBPrio_High);
}

public void sql_CalculatePlayerElo(int I_Client, int I_UId, int I_Style, int I_Type) {
	char C_buffer[512];
	DataPack D_Pack = new DataPack();

	D_Pack.WriteCell(GetClientUserId(I_Client));
	D_Pack.WriteCell(I_UId);
	D_Pack.WriteCell(I_Style);
	D_Pack.WriteCell(I_Type);

	Format(C_buffer, 512, "SELECT mapname FROM `t_records` WHERE uid = '%i' AND style = '%i'", I_UId, I_Style);
	SQL_TQuery(gD_Slave, sqlCalculatePlayerEloCallback, C_buffer, D_Pack, DBPrio_Normal);
}

public void sql_CalculatePlayerEloRank(int I_Client, int I_Style) {
	char C_buffer[512];

	Format(C_buffer, 512, "SELECT * FROM ")
}
