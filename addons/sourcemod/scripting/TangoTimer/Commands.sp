public Action command_test(int I_Client, int I_Arg) {
	SetEntityMoveType(I_Client, MOVETYPE_NOCLIP);

	return Plugin_Handled;
}

public Action command_timer(int I_Client, int I_Args) {
	misc_ClearLocalSettings(I_Client);
	admin_TryNukeData(I_Client);

	admin_Menu(I_Client);
	return Plugin_Handled;
}

public Action command_start(int I_Client, int I_Args) {
	misc_DelayedTeleportClientToZone(I_Client, 0, 0, 0);

	return Plugin_Handled;
}

public Action command_restart(int I_Client, int I_Args) {
	misc_DelayedTeleportClientToZone(I_Client, gI_TimerCurrentZoneGroup[I_Client], 0, 0);

	return Plugin_Handled;
}

public Action command_bstart(int I_Client, int I_Args) {
	char C_Args[32];
	int I_Value;

	if (I_Args < 1) {
		PrintToChat(I_Client, "%s \x07sm_bstart \x10<stage>", PLUGIN_PREFIX);
	} else {
		GetCmdArg(1, C_Args, 32);
		I_Value = StringToInt(C_Args);

		if ((I_Value > 0) && (I_Value < 64)) {
			misc_DelayedTeleportClientToZone(I_Client, I_Value, 0, 0);
		} else {
			PrintToChat(I_Client, "%s Invalid Bonus Number <1/63>", PLUGIN_PREFIX);
		}
	}

	return Plugin_Handled;
}

public Action command_end(int I_Client, int I_Args) {
	misc_DelayedTeleportClientToZone(I_Client, 0, 63, 0);

	return Plugin_Handled;
}

public Action command_bend(int I_Client, int I_Args) {
	char C_Args[32];
	int I_Value;

	if (I_Args < 1) {
		PrintToChat(I_Client, "%s \x07sm_bend \x10<stage>", PLUGIN_PREFIX);
	} else {
		GetCmdArg(1, C_Args, 32);
		I_Value = StringToInt(C_Args);

		if ((I_Value > 0) && (I_Value < 64)) {
			misc_DelayedTeleportClientToZone(I_Client, I_Value, 63, 0);
		} else {
			PrintToChat(I_Client, "%s Invalid Bonus Number <1/63>", PLUGIN_PREFIX);
		}
	}

	return Plugin_Handled;
}

public Action command_stage(int I_Client, int I_Args) {
	char C_Args[32];
	int I_Value;

	if (I_Args < 1) {
		PrintToChat(I_Client, "%s \x07sm_stage \x10<stage>", PLUGIN_PREFIX);
	} else {
		GetCmdArg(1, C_Args, 32);
		I_Value = StringToInt(C_Args);

		if ((I_Value > 0) && (I_Value < 63)) {
			misc_DelayedTeleportClientToZone(I_Client, 0, I_Value, 0);
		} else {
			PrintToChat(I_Client, "%s Invalid Stage Number <1/62>", PLUGIN_PREFIX);
		}
	}

	return Plugin_Handled;
}

public Action command_bstage(int I_Client, int I_Args) {
	char C_Args[2][32];
	int I_Value[2];

	if (I_Args < 2) {
		PrintToChat(I_Client, "%s \x07sm_bstage \x10<bonus> <stage>", PLUGIN_PREFIX);
	} else {
		for (int i = 0; i < 2; i++) {
			GetCmdArg(i + 1, C_Args[i], 32);
			I_Value[i] = StringToInt(C_Args[i]);
		}

		if ((I_Value[0] > 0) && (I_Value[0] < 64)) {
			if ((I_Value[1] > 0) && (I_Value[1] < 63)) {
				misc_DelayedTeleportClientToZone(I_Client, I_Value[0], I_Value[1], 0);
			} else {
				PrintToChat(I_Client, "%s Invalid Stage Number <1/62>", PLUGIN_PREFIX);
			}
		} else {
			PrintToChat(I_Client, "%s Invalid Bonus Number <1/63>", PLUGIN_PREFIX);
		}
	}

	return Plugin_Handled;
}

public Action command_jointeam(int I_Client, int I_Args) {
	char C_Args[32];
	int I_Value;

	GetCmdArg(1, C_Args, 32);
	I_Value = StringToInt(C_Args);

	if (I_Value < 2) {
		ChangeClientTeam(I_Client, I_Value);
	} else {
		ChangeClientTeam(I_Client, 2);
		misc_DelayedTeleportClientToZone(I_Client, 0, 0, 1);
	}

	return Plugin_Handled;
}

public Action command_spec(int I_Client, int I_Args) {
	char C_Args[32];
	int I_Target;

	timer_StopTime(I_Client);

	ChangeClientTeam(I_Client, 1);

	if (I_Args > 0) {
		GetCmdArg(1, C_Args, 32);
		I_Target = FindTarget(0, C_Args, true, false);

		if (I_Target != -1) {
			if (IsPlayerAlive(I_Target)) {
				DataPack D_Pack = new DataPack();
				D_Pack.WriteCell(I_Client);
				D_Pack.WriteCell(I_Target);

				PrintToChat(I_Client, "%s Spectating \x10%N", PLUGIN_PREFIX, I_Target);
				CreateTimer(0.1, Timer_ChangeSpec, D_Pack);
			} else {
				PrintToChat(I_Client, "%s Player \x10%N \x01Is Dead", PLUGIN_PREFIX, I_Target);
			}
		}
	}

	return Plugin_Handled;
}

public Action command_settings(int I_Client, int I_Args) {
	menu_Settings(I_Client);
	return Plugin_Handled;
}

public Action command_newstyle(int I_Client, int I_Args) {
	char C_Args[64], C_SplitArg[2][512], C_SplitString[64][512];
	int I_Style, I_Split;

	I_Style = -1;
	GetCmdArg(0, C_Args, 64);

	ExplodeString(C_Args, "_", C_SplitArg, 2, 512);
	TrimString(C_SplitArg[1]);

	for (int i = 0; i < gI_TotalStyles; i++) {
		I_Split = 0;

		ExplodeString(gI_Styles[i][Aliases], ",", C_SplitString, 64, 512);

		while (strlen(C_SplitString[I_Split]) > 0) {
			TrimString(C_SplitString[I_Split]);

			if (StrEqual(C_SplitArg[1], C_SplitString[I_Split], false)) {
				I_Style = i;
			}

			I_Split++;
		}
	}

	if (I_Style != -1) {
		gI_TimerCurrentStyle[I_Client] = I_Style;
		misc_DelayedTeleportClientToZone(I_Client, gI_TimerCurrentZoneGroup[I_Client], 0, 1);

		PrintToChat(I_Client, "%s Style Now: \x10%s", PLUGIN_PREFIX, gI_Styles[I_Style][StyleName]);
	}

	return Plugin_Handled;
}

public Action command_styles(int I_Client, int I_Args) {
	misc_StyleMenu(I_Client);
	return Plugin_Handled;
}

public Action command_maptop(int I_Client, int I_Args) {
	misc_ClearLocalSettings(I_Client);

	menu_MapTop(I_Client)
	return Plugin_Handled;
}

public Action command_replay(int I_Client, int I_Args) {
	for (int i = 0; i < 2; i++) {
		gI_PlayerReplay[I_Client][i] = 0;
	}

	misc_ReplayMenu(I_Client);
	return Plugin_Handled;
}

public Action command_season(int I_Client, int I_Args) {
	menu_Season(I_Client);
	return Plugin_Handled;
}

public Action command_exp(int I_Client, int I_Args) {
	menu_Exp(I_Client);
	return Plugin_Handled;
}

public Action command_prestige(int I_Client, int I_Args) {
	menu_Prestige(I_Client);
	return Plugin_Handled;
}

public Action command_prestigeshop(int I_Client, int I_Args) {
	menu_PrestigeShop(I_Client);
	return Plugin_Handled;
}

public Action command_welcome(int I_Client, int I_Args) {
	menu_WelcomeMessage(I_Client, 0);
	return Plugin_Handled;
}

public Action command_tag(int I_Client, int I_Args) {
	if (strlen(gC_TimerPlayerPrestigeShopValues[I_Client][2]) > 0) {
		char C_Arg1[32], C_buffer[512], C_Escaped[64];
		GetCmdArg(1, C_Arg1, sizeof(C_Arg1));
		TrimString(C_Arg1);

		if (strlen(C_Arg1) > 10) {
			PrintToChat(I_Client, "%s Sorry, Your entered tag is too long", PLUGIN_PREFIX);
			return Plugin_Handled;
		}

		if (strlen(C_Arg1) == 0) {
			Format(C_Arg1, 32, "-1");
		}

		Format(gC_TimerPlayerPrestigeShopValues[I_Client][2], 512, "%s", C_Arg1);
		PrintToChat(I_Client, "%s Tag set to: %s", PLUGIN_PREFIX, C_Arg1);

		SQL_EscapeString(gD_Slave, C_Arg1, C_Escaped, 64);
		Format(C_buffer, 512, "UPDATE `t_prestigeshop` SET value = '%s' WHERE uid = '%i' AND item = '2'", C_Escaped, gI_TimerPlayerUid[I_Client]);
		gA_SqlQueries.PushString(C_buffer);
	} else {
		PrintToChat(I_Client, "%s Sorry, You need to purchase this through the Prestige Shop!", PLUGIN_PREFIX);
	}

	return Plugin_Handled;
}

public Action command_trails(int I_Client, int I_Args) {
	if (strlen(gC_TimerPlayerPrestigeShopValues[I_Client][3]) > 0) {
		menu_Trails(I_Client);
	} else {
		PrintToChat(I_Client, "%s Sorry, You need to purchase this through the Prestige Shop!", PLUGIN_PREFIX);
	}

	return Plugin_Handled;
}

public Action command_auras(int I_Client, int I_Args) {
	if (strlen(gC_TimerPlayerPrestigeShopValues[I_Client][4]) > 0) {
		menu_Auras(I_Client);
	} else {
		PrintToChat(I_Client, "%s Sorry, You need to purchase this through the Prestige Shop!", PLUGIN_PREFIX);
	}

	return Plugin_Handled;
}

public Action command_hide(int I_Client, int I_Args) {
	gB_TimerPlayerHide[I_Client] = !gB_TimerPlayerHide[I_Client];

	if (gI_TimerPlayerSettings[I_Client][3] == 1) {
		PrintToChat(I_Client, "%s Hide Now: %s", PLUGIN_PREFIX, (gB_TimerPlayerHide[I_Client] ? "\x06Enabled" : "\x07Disabled"));
	}

	return Plugin_Handled;
}

public Action command_noclip(int I_Client, int I_Args) {
	if (!gB_TimerTimerPaused[I_Client] && gB_TimerTimerStarted[I_Client]) {
		timer_PausePlayer(I_Client);
	}

	if (gB_TimerTimerNoclip[I_Client]) {
		SetEntityMoveType(I_Client, MOVETYPE_WALK);

		if (gI_TimerPlayerSettings[I_Client][3] == 1) {
			PrintToChat(I_Client, "%s No-Clip: \x07Disabled", PLUGIN_PREFIX);
		}
	} else {
		SetEntityMoveType(I_Client, MOVETYPE_NOCLIP);

		if (gI_TimerPlayerSettings[I_Client][3] == 1) {
			PrintToChat(I_Client, "%s No-Clip: \x06Enabled", PLUGIN_PREFIX);
		}
	}

	gB_TimerTimerNoclip[I_Client] = !gB_TimerTimerNoclip[I_Client];

	return Plugin_Handled;
}

public Action command_pause(int I_Client, int I_Args) {
	if (!gB_TimerTimerPaused[I_Client] && gB_TimerTimerStarted[I_Client]) {
		timer_PausePlayer(I_Client);

		if (gI_TimerPlayerSettings[I_Client][3] == 1) {
			PrintToChat(I_Client, "%s Paused", PLUGIN_PREFIX);
		}
	} else if (gB_TimerTimerPaused[I_Client] && gB_TimerTimerStarted[I_Client]) {
		timer_ResumePlayer(I_Client);

		if (gI_TimerPlayerSettings[I_Client][3] == 1) {
			PrintToChat(I_Client, "%s Un-Paused", PLUGIN_PREFIX);
		}
	} else if (!gB_TimerTimerStarted[I_Client]) {
		if (gI_TimerPlayerSettings[I_Client][3] == 1) {
			PrintToChat(I_Client, "%s You can't Pause when you don't have a Timer running!", PLUGIN_PREFIX);
		}
	}

	return Plugin_Handled;
}

public Action command_unpause(int I_Client, int I_Args) {
	if (gB_TimerTimerPaused[I_Client] && gB_TimerTimerStarted[I_Client]) {
		timer_ResumePlayer(I_Client);

		if (gI_TimerPlayerSettings[I_Client][3] == 1) {
			PrintToChat(I_Client, "%s Un-Paused", PLUGIN_PREFIX);
		}
	} else {
		if (gI_TimerPlayerSettings[I_Client][3] == 1) {
			PrintToChat(I_Client, "%s You're \x07NOT \x01paused!", PLUGIN_PREFIX);
		}
	}

	return Plugin_Handled;
}

public Action command_jumpstats(int I_Client, int I_Args) {
	menu_JumpStats(I_Client);
	return Plugin_Handled;
}

public Action command_dp(int I_Client, int I_Args) {
	gB_TimerPlayerDisablePickup[I_Client] = !gB_TimerPlayerDisablePickup[I_Client];

	if (gI_TimerPlayerSettings[I_Client][3] == 1) {
		PrintToChat(I_Client, "%s Disable Pickup Now: %s", PLUGIN_PREFIX, (gB_TimerPlayerDisablePickup[I_Client] ? "\x06Enabled" : "\x07Disabled"));
	}

	return Plugin_Handled;
}

public Action command_race(int I_Client, int I_Args) {
	race_MainMenu(I_Client);
	return Plugin_Handled;
}
