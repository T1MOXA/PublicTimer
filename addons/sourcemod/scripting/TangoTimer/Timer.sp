public void timer_ClearData(int I_Client) {
	gI_TimerCurrentZone[I_Client] = 0;
	gI_TimerCurrentZoneGroup[I_Client] = 0;
	gI_TimerCurrentCheckpointStage[I_Client] = 0;
	gI_TimerCurrentStyle[I_Client] = 0;
	gI_TimerCurrentJumps[I_Client] = 0;
	gI_TimerCurrentJumpInterval[I_Client] = 0;

	gI_TimerPlayerUid[I_Client] = 0;
	gI_TimerCurrentTokens[I_Client] = 0;
	gI_TimerCurrentSeason[I_Client] = 0;

	for (int i = 0; i < 5; i++) {
		Format(gC_TimerPlayerPrestigeShopValues[I_Client][i], 512, "");
	}

	Format(gC_TimerPlayerSteamId[I_Client], 64, "INVALID_ID");
	Format(gC_TimerPlayerIp[I_Client], 64, "INVALID_IP");

	for (int i = 0; i < 2; i++) {
		gI_PlayerReplay[I_Client][i] = 0;

		gI_TimerCurrentSync[I_Client][i] = 0;
		gI_TimerPlayerVip[I_Client][i] = 0;
	}

	gF_TimerCurrentSpeed[I_Client] = 0.0;

	for (int i = 0; i < 3; i++) {
		gF_TimerLastAngles[I_Client][i] = 0.0;
		gI_TimerPlayerItems[I_Client][i] = 0;
	}

	for (int i = 0; i < 64; i++) {
		for (int x = 0; x < gI_TotalStyles; x++) {
			for (int z = 0; z < SEASON; z++) {
				gI_TimerPlayerAttempts[I_Client][z][i][x] = 0;
			}
			gI_TimerPlayerRecords[I_Client][i][x] = 0;

			for (int y = 0; y < 64; y++) {
				gI_TimerPlayerCheckpointRecords[I_Client][i][x][y] = 0;
			}
		}

		gI_TimerMiscMultipleSettings[I_Client][i] = -1;
	}

	for (int i = 0; i < 3; i++) {
		for (int x = 0; x < SEASON; x++) {
			gI_TimerPlayerExp[I_Client][x][i] = 0;
			gI_TimerPlayerRaces[I_Client][x][i] = 0;
		}
	}

	gI_TimerPlayerLevel[I_Client] = 0;

	for (int i = 0; i < TOTAL_SETTINGS; i++) {
		gI_TimerPlayerSettings[I_Client][i] = 0;
	}

	gF_TimerStartTime[I_Client] = 0.0;
	gF_TimerCheckpointTime[I_Client] = 0.0;

	gB_TimerInBonus[I_Client] = false;
	gB_TimerPlayerNew[I_Client] = false;
	gB_TimerTimerStarted[I_Client] = false;
	gB_TimerRecentlyAbused[I_Client] = false;
	gB_TimerPlayerLoaded[I_Client] = false;
	gB_TimerPlayerHide[I_Client] = false;
	gB_TimerPlayerDisablePickup[I_Client] = false;

	for (int i = 0; i < gI_TotalStyles; i++) {
		gI_TimerPlayerElo[I_Client][i] = 0;
		gF_TimerPlayerEloValue[I_Client][i] = 0.0;
	}

	delete gA_TimerCheckpointData[I_Client];
	gA_TimerCheckpointData[I_Client] = new ArrayList();

	delete gA_TimerJumpStats[I_Client];
	gA_TimerJumpStats[I_Client] = new ArrayList(3);

	replay_Clear(I_Client);
	timer_ResetPause(I_Client);
}

public void timer_StartTime(int I_Client, int I_Index) {
	if (gB_TimerPlayerLoaded[I_Client]) {
		int I_ZoneGroup;
		I_ZoneGroup = gA_Zones.Get(I_Index, 2);

		gI_TimerCurrentZoneGroup[I_Client] = I_ZoneGroup;
		gI_TimerCurrentCheckpointStage[I_Client] = 0;
		gF_TimerStartTime[I_Client] = GetGameTime();
		gF_TimerCheckpointTime[I_Client] = 0.0;
		gB_TimerTimerStarted[I_Client] = true;

		delete gA_TimerCheckpointData[I_Client];
		gA_TimerCheckpointData[I_Client] = new ArrayList();

		/*
		if (gI_TimerCurrentJumpInterval[I_Client] > 0) {
			if (gA_TimerJumpStats[I_Client].Length == 0 && (GetEntityFlags(I_Client) & FL_ONGROUND)) {
				float F_Speed, F_Velocity[3];

				GetEntPropVector(I_Client, Prop_Data, "m_vecVelocity", F_Velocity);
				F_Speed = SquareRoot(Pow(F_Velocity[0], 2.0) + Pow(F_Velocity[1], 2.0));

				gI_TimerCurrentJumps[I_Client] = 1;

				gA_TimerJumpStats[I_Client].Resize(gI_TimerCurrentJumps[I_Client] + 1);

				gA_TimerJumpStats[I_Client].Set(gI_TimerCurrentJumps[I_Client], F_Speed, 0);
				gA_TimerJumpStats[I_Client].Set(gI_TimerCurrentJumps[I_Client], gI_TimerCurrentSync[I_Client][0], 1);
				gA_TimerJumpStats[I_Client].Set(gI_TimerCurrentJumps[I_Client], gI_TimerCurrentSync[I_Client][1], 2);
			}
		}
		*/

		replay_Clear(I_Client);
		timer_ResetPause(I_Client);
	} else {
		PrintToChat(I_Client, "%s Data is still being loaded! Timer not started!", PLUGIN_PREFIX);
	}
}

public void timer_StopTime(int I_Client) {
	gI_TimerCurrentCheckpointStage[I_Client] = 0;
	gF_TimerStartTime[I_Client] = 0.0;
	gF_TimerCheckpointTime[I_Client] = 0.0;
	gB_TimerTimerStarted[I_Client] = false;
	gI_TimerCurrentJumps[I_Client] = 0;

	delete gA_TimerCheckpointData[I_Client];
	gA_TimerCheckpointData[I_Client] = new ArrayList();

	delete gA_TimerJumpStats[I_Client];
	gA_TimerJumpStats[I_Client] = new ArrayList(3);

	replay_Clear(I_Client);
	timer_ResetPause(I_Client);
}

public void timer_PlayerCheckpoint(int I_Client, int I_Index) {
	int I_ZoneGroup, I_Style, I_Jumps, I_Time, I_PBTime, I_WRTime, I_CheckpointStage;

	I_ZoneGroup = gI_TimerCurrentZoneGroup[I_Client];
	I_Style = gI_TimerCurrentStyle[I_Client];
	I_Jumps = gI_TimerCurrentJumps[I_Client];

	I_Time = RoundToFloor((GetGameTime() - gF_TimerStartTime[I_Client]) * 1000);
	I_PBTime = gI_TimerPlayerCheckpointRecords[I_Client][I_ZoneGroup][I_Style][gI_TimerCurrentCheckpointStage[I_Client]];
	I_WRTime = gI_TimerPlayerCheckpointRecords[0][I_ZoneGroup][I_Style][gI_TimerCurrentCheckpointStage[I_Client]];

	gA_TimerCheckpointData[I_Client].Resize(gI_TimerCurrentCheckpointStage[I_Client] + 1);
	gA_TimerCheckpointData[I_Client].Set(gI_TimerCurrentCheckpointStage[I_Client], I_Time);
	gI_TimerCurrentCheckpointStage[I_Client]++;

	I_CheckpointStage = gI_TimerCurrentCheckpointStage[I_Client];

	gF_TimerCheckpointTime[I_Client] = GetGameTime();

	misc_Message(I_Client, I_ZoneGroup, I_Style, I_Jumps, I_Time, I_PBTime, I_WRTime, I_CheckpointStage, 0);
}

public void timer_PlayerFinished(int I_Client, int I_Index) {
	int I_ZoneGroup, I_Style, I_Jumps, I_Time, I_PBTime, I_WRTime, I_TimeStamp;
	float F_Sync;
	ArrayList A_CheckpointData = CreateArray();

	I_ZoneGroup = gI_TimerCurrentZoneGroup[I_Client];
	I_Style = gI_TimerCurrentStyle[I_Client];
	I_Jumps = gI_TimerCurrentJumps[I_Client];

	F_Sync = float(gI_TimerCurrentSync[I_Client][0]) / float(gI_TimerCurrentSync[I_Client][1] + 1) * 100.0;

	I_Time = RoundToFloor((GetGameTime() - gF_TimerStartTime[I_Client]) * 1000);
	I_PBTime = gI_TimerPlayerRecords[I_Client][I_ZoneGroup][I_Style];
	I_WRTime = gI_TimerPlayerRecords[0][I_ZoneGroup][I_Style];

	I_TimeStamp = GetTime();

	if ((I_WRTime == 0) || (I_Time < I_WRTime)) {
		gI_TimerPlayerRecords[0][I_ZoneGroup][I_Style] = I_Time;

		for (int i = 0; i < gA_TimerCheckpointData[I_Client].Length; i++) {
			gI_TimerPlayerCheckpointRecords[0][I_ZoneGroup][I_Style][i] = gA_TimerCheckpointData[I_Client].Get(i)
		}
	}

	if ((I_PBTime == 0) || (I_Time < I_PBTime)) {
		for (int i = 0; i < gA_TimerCheckpointData[I_Client].Length; i++) {
			A_CheckpointData.Push(gA_TimerCheckpointData[I_Client].Get(i));
		}

		if (I_PBTime == 0) {
			sql_InsertRecord(I_Client, I_Time, F_Sync, I_Jumps, I_Style, I_ZoneGroup, I_TimeStamp, A_CheckpointData);
		} else if (I_Time < I_PBTime) {
			sql_UpdateRecord(I_Client, I_Time, F_Sync, I_Jumps, I_Style, I_ZoneGroup, I_TimeStamp, A_CheckpointData);
		}

		misc_AlterLocalDB(I_Client, I_Time, F_Sync, I_Jumps, I_Style, I_ZoneGroup, I_TimeStamp);

		gI_TimerPlayerRecords[I_Client][I_ZoneGroup][I_Style] = I_Time;

		for (int i = 0; i < gA_TimerCheckpointData[I_Client].Length; i++) {
			gI_TimerPlayerCheckpointRecords[I_Client][I_ZoneGroup][I_Style][i] = gA_TimerCheckpointData[I_Client].Get(i)
		}
	} else {
		delete A_CheckpointData;
	}

	misc_Exp(I_Client, I_ZoneGroup, I_Style);

	if (gI_TimerPlayerAttempts[I_Client][gI_TimerCurrentSeason[I_Client]][I_ZoneGroup][I_Style] == 0) {
		sql_InsertAttempts(I_Client, I_Style, I_ZoneGroup);
	} else {
		sql_UpdateAttempts(I_Client, I_Style, I_ZoneGroup);
	}

	gI_TimerPlayerAttempts[I_Client][gI_TimerCurrentSeason[I_Client]][I_ZoneGroup][I_Style]++;

	misc_Message(I_Client, I_ZoneGroup, I_Style, I_Jumps, I_Time, I_PBTime, I_WRTime, 0, 1);
	replay_TrySaveReplay(I_Client, I_ZoneGroup, I_Style, I_Time);
}

public void timer_PausePlayer(int I_Client) {
	gF_TimerTimerPauseTime[I_Client] = GetGameTime();
	gB_TimerTimerPaused[I_Client] = true;

	GetClientAbsOrigin(I_Client, gF_TimerTimerPause[I_Client][0]);
	GetClientEyeAngles(I_Client, gF_TimerTimerPause[I_Client][1]);
	GetEntPropVector(I_Client, Prop_Data, "m_vecVelocity", gF_TimerTimerPause[I_Client][2]);

	SetEntProp(I_Client, Prop_Data, "m_CollisionGroup", COLLISION_GROUP_DEBRIS);
	misc_RecentlyAbused(I_Client, 0.5);
}

public void timer_ResumePlayer(int I_Client) {
	gF_TimerStartTime[I_Client] += (GetGameTime() - gF_TimerTimerPauseTime[I_Client]);

	misc_RecentlyAbused(I_Client, 0.5);
	TeleportEntity(I_Client, gF_TimerTimerPause[I_Client][0], gF_TimerTimerPause[I_Client][1], gF_TimerTimerPause[I_Client][2]);

	timer_ResetPause(I_Client);
}

public void timer_ResetPause(int I_Client) {
	for (int i = 0; i < 3; i++) {
		for (int x = 0; x < 3; x++) {
			gF_TimerTimerPause[I_Client][i][x] = 0.0;
		}
	}

	gF_TimerTimerPauseTime[I_Client] = 0.0;

	gB_TimerTimerPaused[I_Client] = false;
	gB_TimerTimerNoclip[I_Client] = false;

	SetEntityMoveType(I_Client, MOVETYPE_WALK);
	SetEntProp(I_Client, Prop_Data, "m_CollisionGroup", COLLISION_GROUP_DEBRIS_TRIGGER);
}
