#define PLUGIN_VERSION "4.182"
#define PLUGIN_PREFIX "[\x09Timer\x01]"
#define REPLAY_PREFIX "[\x10Replay\x01]"
#define MENU_PREFIX "Timer"
#define PLUGIN_DEBUG "[TT DEBUG]"
#define SQL_PREFIX "[Timer] SQL Error"
#define PERCENT "%%"

#define SEASON_EPOCH_START 1483228800

//Minimum 2.
#define TOTAL_BOTS 2

#define TOTAL_STYLES 24
#define TOTAL_SETTINGS 8
#define TOTAL_TITLES 16
#define SEASON 1

#define EXP_MULTIPLIER 1.0

#include <sourcemod>
#include <sdkhooks>
#include <sdktools>
#include <cstrike>
#include <smlib>
#include <scp>

#include "TangoTimer/Variables.sp"
#include "TangoTimer/Sql.sp"
#include "TangoTimer/SqlCallbacks.sp"
#include "TangoTimer/Zone.sp"
#include "TangoTimer/Admin.sp"
#include "TangoTimer/Timers.sp"
#include "TangoTimer/Timer.sp"
#include "TangoTimer/Misc.sp"
#include "TangoTimer/Commands.sp"
#include "TangoTimer/Hooks.sp"
#include "TangoTimer/Replays.sp"
#include "TangoTimer/Menu.sp"
#include "TangoTimer/Races.sp"
//#include "TangoTimer/Triggers.sp"

public Plugin myinfo = {
	name = "Instinct Timer",
	author = "Oscar Wos (OSWO)",
	description = "Timer - Surf/BunnyHop",
	version = PLUGIN_VERSION,
	url = "",
};

public void OnPluginStart() {
	ConVar Cn_ip = FindConVar("ip");
	Cn_ip.GetString(gC_ServerIp, 64);
	GetCurrentMap(gC_CurrentMap, 128);

	gA_Zones = new ArrayList(10);
	gA_SqlQueries = new ArrayList(512);

	gA_Names = new ArrayList(128);
	gA_SteamIds = new ArrayList(64);
	gA_ReplayQueue = new ArrayList(3);

	for (int i = 0; i < 3; i++) {
		gA_Whitelists[i] = new ArrayList(128);
	}

	for (int i = 0; i < 64; i++) {
		for (int x = 0; x < gI_TotalStyles; x++) {
			gA_MapRecords[i][x] = new ArrayList(7);
		}

		gA_TimerCheckpointData[i] = new ArrayList();
		gA_TimerPlayerFrames[i] = new ArrayList(6);
		gA_TimerJumpStats[i] = new ArrayList(3);
	}

	for (int i = 0; i < TOTAL_BOTS; i++) {
		gA_ReplayCloneFrames[i] = new ArrayList(6);
	}

	misc_LoadStyles();
	misc_LoadTitles();
	misc_ConVars();

	sql_PluginStart();
	misc_LoadWhitelists();
	replay_PluginStart();

	RegConsoleCmd("jointeam", command_jointeam);
	RegConsoleCmd("sm_test", command_test);
	RegConsoleCmd("sm_timer", command_timer);

	RegConsoleCmd("sm_dp", command_dp);

	RegConsoleCmd("sm_spec", command_spec);
	RegConsoleCmd("sm_spectate", command_spec);

	RegConsoleCmd("sm_restart", command_restart);
	RegConsoleCmd("sm_r", command_restart);

	RegConsoleCmd("sm_start", command_start);
	RegConsoleCmd("sm_s", command_start);
	RegConsoleCmd("sm_normal", command_start);
	RegConsoleCmd("sm_n", command_start);

	RegConsoleCmd("sm_bstart", command_bstart);
	RegConsoleCmd("sm_bonus", command_bstart);
	RegConsoleCmd("sm_b", command_bstart);

	RegConsoleCmd("sm_stage", command_stage);
	RegConsoleCmd("sm_bstage", command_bstage);

	RegConsoleCmd("sm_end", command_end);
	RegConsoleCmd("sm_bend", command_bend);

	RegConsoleCmd("sm_settings", command_settings);

	RegConsoleCmd("sm_maptop", command_maptop);
	RegConsoleCmd("sm_mtop", command_maptop);
	RegConsoleCmd("sm_wr", command_maptop);

	RegConsoleCmd("sm_replay", command_replay);

	RegConsoleCmd("sm_exp", command_exp);

	RegConsoleCmd("sm_styles", command_styles);
	RegConsoleCmd("sm_style", command_styles);

	RegConsoleCmd("sm_pause", command_pause);
	RegConsoleCmd("sm_p", command_pause);

	RegConsoleCmd("sm_nc", command_noclip);
	RegConsoleCmd("sm_noclip", command_noclip);

	RegConsoleCmd("sm_unpause", command_unpause);
	RegConsoleCmd("sm_resume", command_unpause);

	RegConsoleCmd("sm_season", command_season);

	RegConsoleCmd("sm_prestige", command_prestige);
	RegConsoleCmd("sm_prestigeshop", command_prestigeshop);

	RegConsoleCmd("sm_hide", command_hide);

	RegConsoleCmd("sm_ssj", command_jumpstats);
	RegConsoleCmd("sm_js", command_jumpstats);

	RegConsoleCmd("sm_tag", command_tag);
	RegConsoleCmd("sm_aura", command_auras);
	RegConsoleCmd("sm_auras", command_auras);
	RegConsoleCmd("sm_trail", command_trails);
	RegConsoleCmd("sm_trails", command_trails);

	RegConsoleCmd("sm_race", command_race);
	RegConsoleCmd("sm_races", command_race);

	RegConsoleCmd("sm_welcome", command_welcome);

	HookEvent("round_start", HookEvent_RoundStartPre, EventHookMode_Pre);
	HookEvent("round_start", HookEvent_RoundStartPost, EventHookMode_Post);
	HookEvent("round_end", HookEvent_RoundEnd, EventHookMode_Pre);

	HookEvent("round_freeze_end", HookEvent_RoundFreezeEnd, EventHookMode_Pre);

	HookEvent("game_end", HookEvent_GameEnd, EventHookMode_Pre);

	HookEvent("player_spawn", HookEvent_PlayerSpawn, EventHookMode_Post);
	HookEvent("player_death", HookEvent_PlayerDeath, EventHookMode_Pre);

	HookEvent("player_team", HookEvent_PlayerTeam, EventHookMode_Pre);
	HookEvent("player_jump", HookEvent_PlayerJump);

	HookEvent("player_death", HookEvent_PlayerDeath, EventHookMode_Pre);

	HookEvent("player_connect", HookEvent_PlayerConnect, EventHookMode_Pre);
	HookEvent("player_disconnect", HookEvent_PlayerDisconnect, EventHookMode_Pre);

	HookUserMessage(GetUserMessageId("TextMsg"), UserMsg_TextMsg, true);
	HookUserMessage(GetUserMessageId("SayText2"), UserMsg_SayText2, true);
	LoadTranslations("common.phrases");

	AddCommandListener(HookEvent_Chat, "say");
	AddCommandListener(HookEvent_Chat, "say_team");

	gCn_AutoBunnyHopping = FindConVar("sv_autobunnyhopping");
	AddTempEntHook("Shotgun Shot", HookSound_ShotgunShot);

	//HookEntityOutput("trigger_push", "OnStartTouch", HookEntity_TriggerPushOnTrigger);
	//HookEntityOutput("trigger_push", "OnEndTouch", HookEntity_TriggerPushOnTrigger);
	//HookEntityOutput("trigger_push", "OnTrigger", HookEntity_TriggerPushOnTrigger);

	CreateTimer(1.0, Timer_LoadClients, _);
}

public void OnMapEnd() {
	sql_LogMapEnd();

	zone_ZoneEnd();
	replay_ClearBots();

	timer_ClearData(0);
}

public void OnPluginEnd() {
	zone_EntityClear();
	sql_LogEnd();
}

public void OnMapStart() {
	char C_Path[512];

	GetCurrentMap(gC_CurrentMap, 128);
	Format(C_Path, 512, "maps/%s.nav", gC_CurrentMap);

	if (!FileExists(C_Path)) {
		File_Copy("maps/replay.nav", C_Path);
	}

	misc_Precache();
	misc_LoadItems();

	sql_LogMapStart();

	sql_LoadTables();
	sql_LoadMapInfo();
	sql_LoadZones();

	sql_LoadMapRecords();
	replay_LoadAllReplays();
	GetCurrentMap(gC_CurrentMap, 128);

	gI_RainbowColors[0] = 255;
	gI_RainbowColors[1] = 24;
	gI_RainbowColors[2] = 24;

	gB_ChangingMaps = false;

	gH_ZoneTimer = CreateTimer(0.1, Timer_Zones, _, TIMER_REPEAT && TIMER_FLAG_NO_MAPCHANGE);
	gH_SecondTimer = CreateTimer(1.0, Timer_Second, _, TIMER_REPEAT && TIMER_FLAG_NO_MAPCHANGE);

	ServerCommand("mp_restartgame 1");
	replay_SpawnBots();
	misc_ConVars();
}

public bool OnClientConnect(int I_Client) {
	char C_ClanTag[128], C_NameTag[256];

	Format(C_ClanTag, 128, "[Replay]");
	Format(C_NameTag, 256, "Use !replay");

	if (IsFakeClient(I_Client)) {
		for (int i = 0; i < TOTAL_BOTS; i++) {
			if (gI_ReplayBotIds[i] == -1) {
				gI_ReplayBotTicks[i] = -1;
				gI_ReplayBotIds[i] = I_Client;

				if (i == 0) {
					CreateTimer(1.0, Timer_ResetAutoBot);
				}

				if (i > 0) {
					CS_SetClientClanTag(I_Client, C_ClanTag);
					SetClientInfo(I_Client, "name", C_NameTag);
				}

				break;
			}
		}
	}

	return true;
}

public void OnClientPostAdminCheck(int I_Client) {
	timer_ClearData(I_Client);

	GetClientAuthId(I_Client, AuthId_SteamID64, gC_TimerPlayerSteamId[I_Client], 64, true);
	GetClientIP(I_Client, gC_TimerPlayerIp[I_Client], 64, true);

	StripQuotes(gC_TimerPlayerSteamId[I_Client]);
	StripQuotes(gC_TimerPlayerIp[I_Client]);

	for (int i = 0; i < sizeof(gC_Blacklist); i++) {
		if (StrEqual(gC_Blacklist[i], gC_TimerPlayerSteamId[I_Client], false)) {
			KickClient(I_Client, "Sorry, You're on the blacklist for a reason. Unlucky.");
			return;
		}
	}

	if (IsValidClient(I_Client)) {
		sql_LogPlayerConnect(I_Client);

		CreateTimer(0.1, Timer_PostAdminCheck, GetClientUserId(I_Client), TIMER_REPEAT);
	}

	SDKHook(I_Client, SDKHook_SetTransmit, Hook_OnTransmit);
	SDKHook(I_Client, SDKHook_OnTakeDamage, Hook_OnTakeDamageCallback);
	SDKHook(I_Client, SDKHook_WeaponDropPost, Hook_OnWeaponDropPostCallback);
	SDKHook(I_Client, SDKHook_WeaponCanUse, Hook_WeaponCanUse);
}

public void OnClientDisconnect(int I_Client) {
	if (IsValidClient(I_Client)) {
		sql_LogPlayerDisconnect(I_Client);
		sql_PlayerDisconnect(I_Client);
		admin_TryNukeData(I_Client);

		timer_ClearData(I_Client);
	}

	SDKUnhook(I_Client, SDKHook_SetTransmit, Hook_OnTransmit);
	SDKUnhook(I_Client, SDKHook_OnTakeDamage, Hook_OnTakeDamageCallback);
	SDKUnhook(I_Client, SDKHook_WeaponDropPost, Hook_OnWeaponDropPostCallback);
	SDKUnhook(I_Client, SDKHook_WeaponCanUse, Hook_WeaponCanUse);
}

public Action CS_OnTerminateRound(float &delay, CSRoundEndReason &reason) {
	if (gB_ChangingMaps) {
		return Plugin_Continue;
	}

	return Plugin_Handled;
}

public Action OnPlayerRunCmd(int I_Client, int &I_Buttons, int &I_Impulse, float F_Vel[3], float F_Angles[3], int &I_Weapon, int &I_Subtype, int &I_CmdNum, int &I_TickCout, int &I_Seed, int I_Mouse[2]) {
	int I_Target, I_CurrentZone, I_CurrentStyle;
	float F_PlayerVelocity[3], F_Scale;
	bool B_ButtonError;

	if (IsValidClient(I_Client)) {
		replay_PlayerCmd(I_Client, F_Angles, I_Buttons);

		if (IsClientObserver(I_Client)) {
			int I_ClientSpecMode = GetEntProp(I_Client, Prop_Send, "m_iObserverMode");

			if (I_ClientSpecMode == 4 || I_ClientSpecMode == 5) {
				I_Target = GetEntPropEnt(I_Client, Prop_Send, "m_hObserverTarget");

				if (IsValidClient(I_Target)) {
					misc_ShowHud(I_Client, I_Target);
				}
			}
		} else {
			misc_ShowHud(I_Client, I_Client);

			GetEntPropVector(I_Client, Prop_Data, "m_vecVelocity", F_PlayerVelocity);

			I_CurrentZone = gI_TimerCurrentZone[I_Client];
			I_CurrentStyle = gI_TimerCurrentStyle[I_Client];
			gF_TimerCurrentSpeed[I_Client] = SquareRoot(Pow(F_PlayerVelocity[0], 2.0) + Pow(F_PlayerVelocity[1], 2.0));

			if (GetEntityMoveType(I_Client) != MOVETYPE_NOCLIP) {
				if (((I_CurrentZone == 0) || (I_CurrentZone == 3)) && (gI_Styles[I_CurrentStyle][PreSpeed] != 0.0) && (gF_TimerCurrentSpeed[I_Client] > gI_Styles[I_CurrentStyle][PreSpeed])) {
					F_Scale = (gI_Styles[I_CurrentStyle][PreSpeed] / gF_TimerCurrentSpeed[I_Client]) * 0.9;

					ScaleVector(F_PlayerVelocity, F_Scale);
					TeleportEntity(I_Client, NULL_VECTOR, NULL_VECTOR, F_PlayerVelocity);
				}

				if ((gB_TimerTimerStarted[I_Client]) && (gI_Styles[I_CurrentStyle][MaxSpeed] != 0.0) && (gF_TimerCurrentSpeed[I_Client] > gI_Styles[I_CurrentStyle][MaxSpeed])) {
					F_Scale = (gI_Styles[I_CurrentStyle][MaxSpeed] / gF_TimerCurrentSpeed[I_Client]);

					ScaleVector(F_PlayerVelocity, F_Scale);
					TeleportEntity(I_Client, NULL_VECTOR, NULL_VECTOR, F_PlayerVelocity);
				}

				if (gI_Styles[I_CurrentStyle][AutoBhop]) {
					if (IsPlayerAlive(I_Client) && (I_Buttons & IN_JUMP)) {
						if (((I_CurrentZone == 0) || (I_CurrentZone == 3)) && !gI_Styles[I_CurrentStyle][StartBhop]) {
							SendConVarValue(I_Client, gCn_AutoBunnyHopping, "0");
						} else {

							if (!(GetEntityMoveType(I_Client) & MOVETYPE_LADDER) && !(GetEntityFlags(I_Client) & FL_ONGROUND) && (GetEntProp(I_Client, Prop_Data, "m_nWaterLevel") < 2)) {
								I_Buttons &= ~IN_JUMP;
							}
						}
					}
				}

				if (gB_TimerTimerStarted[I_Client] && !gB_TimerTimerPaused[I_Client]) {
					if (((I_Buttons & IN_FORWARD) || (I_Buttons & IN_BACK)) && (F_Vel[0] == 0.0)) {
						B_ButtonError = true;
					} else if (((I_Buttons & IN_MOVELEFT) || (I_Buttons & IN_MOVERIGHT)) && (F_Vel[1] == 0.0)) {
						B_ButtonError = true;
					}

					if (!(GetEntityMoveType(I_Client) & MOVETYPE_LADDER) && !(GetEntityFlags(I_Client) & FL_ONGROUND) && (GetEntProp(I_Client, Prop_Data, "m_nWaterLevel") < 2)) {
						if ((I_Buttons & IN_MOVELEFT) && gI_Styles[I_CurrentStyle][PreventLeft]) {
							B_ButtonError = true;
						} else if ((I_Buttons & IN_MOVERIGHT) && gI_Styles[I_CurrentStyle][PreventRight]) {
							B_ButtonError = true;
						} else if ((I_Buttons & IN_FORWARD) && gI_Styles[I_CurrentStyle][PreventForward]) {
							B_ButtonError = true;
						} else if ((I_Buttons & IN_BACK) && gI_Styles[I_CurrentStyle][PreventBack]) {
							B_ButtonError = true;
						}

						if (StrEqual(gI_Styles[I_CurrentStyle][SpecialId], gC_SpecialStyles[0], false)) { //hsw
							if (((I_Buttons & IN_FORWARD) && (I_Buttons & IN_MOVELEFT)) || ((I_Buttons & IN_FORWARD) && (I_Buttons & IN_MOVERIGHT))) {

							} else {
								B_ButtonError = true;
							}
						} else if (StrEqual(gI_Styles[I_CurrentStyle][SpecialId], gC_SpecialStyles[1], false)) { //shsw
							if (((I_Buttons & IN_FORWARD) && (I_Buttons & IN_MOVELEFT)) || ((I_Buttons & IN_BACK) && (I_Buttons & IN_MOVERIGHT))) {

							} else {
								B_ButtonError = true;
							}
						}
					}

					if (gI_Styles[I_CurrentStyle][Sync]) {
						if (!(GetEntityFlags(I_Client) & FL_ONGROUND)) {
							float F_AngleDiff = F_Angles[1] - gF_TimerLastAngles[I_Client][1];

							if (F_AngleDiff > 180.0) {
								F_AngleDiff -= 360.0;
							} else if (F_AngleDiff < -180.0) {
								F_AngleDiff += 360.0;
							}

							if (F_AngleDiff != 0.0) {
								gI_TimerCurrentSync[I_Client][1] += 1;
							}

							if (F_AngleDiff > 0.0) {
								if ((I_Buttons & IN_MOVELEFT) && !(I_Buttons & IN_MOVERIGHT)) {
									gI_TimerCurrentSync[I_Client][0] += 1;
								}
							} else if (F_AngleDiff < 0.0) {
								if ((I_Buttons & IN_MOVERIGHT) && !(I_Buttons & IN_MOVELEFT)) {
									gI_TimerCurrentSync[I_Client][0] += 1;
								}
							}

							gF_TimerLastAngles[I_Client][1] = F_Angles[1];
						}
					}
				} else {
					if (!gB_TimerTimerPaused[I_Client]) {
						gI_TimerCurrentSync[I_Client][0] = 0;
						gI_TimerCurrentSync[I_Client][1] = 0;
					}
				}

				if (B_ButtonError) {
					for (int i = 0; i < 2; i++) {
						F_Vel[i] = 0.0;
					}
				}
			}
		}
	} else {
		float F_Pos[3], F_NewPos[3], F_NewAngles[3], F_Velocity[3];
		char C_buffer[512];

		for (int i = 0; i < TOTAL_BOTS; i++) {
			if (I_Client == gI_ReplayBotIds[i]) {
				SetEntProp(I_Client, Prop_Data, "m_CollisionGroup", 2);
				GetClientAbsOrigin(I_Client, F_Pos);

				if (gI_ReplayBotTicks[i] > -1) {
					if (gI_ReplayBotTicks[i] < gA_ReplayCloneFrames[i].Length) {
						for (int x = 0; x < 3; x++) {
							F_NewPos[x] = gA_ReplayCloneFrames[i].Get(gI_ReplayBotTicks[i], x);
						}

						for (int x = 0; x < 2; x++) {
							F_NewAngles[x] = gA_ReplayCloneFrames[i].Get(gI_ReplayBotTicks[i], x + 3);
						}

						I_Buttons = gA_ReplayCloneFrames[i].Get(gI_ReplayBotTicks[i], 5);

						MakeVectorFromPoints(F_Pos, F_NewPos, F_Velocity);
						ScaleVector(F_Velocity, (1.0 / GetTickInterval()));

						gF_TimerCurrentSpeed[gI_ReplayBotIds[i]] = SquareRoot(Pow(F_Velocity[0], 2.0) + Pow(F_Velocity[1], 2.0));
						SetEntityMoveType(I_Client, ((GetEntityFlags(I_Client) & FL_ONGROUND) > 0) ? MOVETYPE_WALK:MOVETYPE_NOCLIP);

						gI_ReplayBotTicks[i]++;

						if (gI_ReplayBotTicks[i] == 1) {
							TeleportEntity(I_Client, F_NewPos, F_NewAngles, view_as<float>({0.0, 0.0, 0.0}));
						} else if (gI_ReplayBotTicks[i] + 1 > (gA_ReplayCloneFrames[i].Length)) {
							SetEntityMoveType(I_Client, MOVETYPE_NONE);
							TeleportEntity(I_Client, NULL_VECTOR, NULL_VECTOR, view_as<float>({0.0, 0.0, 0.0}));
						} else {
							TeleportEntity(I_Client, NULL_VECTOR, F_NewAngles, F_Velocity);
						}
					} else {
						delete gA_ReplayCloneFrames[i];
						gA_ReplayCloneFrames[i] = new ArrayList(6);

						gI_ReplayBotTicks[i] = -1;

						if (i == 0) {
							CreateTimer(5.0, Timer_ResetAutoBot);
						} else if (i > 0) {
							int I_ZoneGroup, I_Style;

							I_ZoneGroup = gI_ReplayCurrentlyReplaying[i - 1][0];
							I_Style = gI_ReplayCurrentlyReplaying[i - 1][1];

							if (I_ZoneGroup == 0) {
								Format(C_buffer, 512, "\x0BN\x01");
							} else {
								Format(C_buffer, 512, "\x0EB%i\x01", I_ZoneGroup);
							}

							Format(C_buffer, 512, "%s Bot: \x0E#%i \x01Finished - [%s] on [\x03%s\x01]", REPLAY_PREFIX, i, C_buffer, gI_Styles[I_Style][StylePrefix]);
							misc_PrintToChatAll(C_buffer, true, 3);

							CreateTimer(3.0, Timer_CheckBotQueue, i);
						}
					}
				}
			}
		}
	}

	return Plugin_Changed;
}

public Action OnChatMessage(int &author, Handle recipients, char[] name, char[] message) {
	char C_buffer[512];

	for (int i = 0; i < sizeof(gC_DevList); i++) {
		if (StrEqual(gC_DevList[i], gC_TimerPlayerSteamId[author], false)) {
			Format(C_buffer, 512, "\x03[\x0eα\x03] ");
			break;
		}
	}

	if (gI_TimerPlayerVip[author][0] > 0) {
		switch (gI_TimerPlayerVip[author][0]) {
			case 1: {
				Format(C_buffer, 512, "%s\x03[\x06★\x03] ", C_buffer);
			} case 2: {
				Format(C_buffer, 512, "%s\x03[\x10★\x03] ", C_buffer)
			} case 3: {
				Format(C_buffer, 512, "%s\x03[\x07★\x03] ", C_buffer)
			}
		}
	}

	if (strlen(gC_TimerPlayerPrestigeShopValues[author][2]) > 0) {
		if (StrEqual(gC_TimerPlayerPrestigeShopValues[author][2], "-1", false)) {
			Format(C_buffer, 512, "%s\x03[\x01Tag\x03] ", C_buffer);
		} else {
			Format(C_buffer, 512, "%s\x03[\x01%s\x03] ", C_buffer, gC_TimerPlayerPrestigeShopValues[author][2]);
		}
	}

	Format(name, 512, "%s%s", C_buffer, name);
	return Plugin_Changed;
}

stock bool IsValidClient(int I_Client) {
	if (I_Client >= 1 && (I_Client <= MaxClients) && IsValidEntity(I_Client) && IsClientConnected(I_Client) && IsClientInGame(I_Client) && !IsFakeClient(I_Client)) {
		return true;
	}

	return false;
}

stock void PrecacheParticleEffect(const char[] C_EffectName) {
	static int I_Table = INVALID_STRING_TABLE;

	if (I_Table == INVALID_STRING_TABLE) {
		I_Table = FindStringTable("ParticleEffectNames");
	}

	bool B_Save = LockStringTables(false);
	AddToStringTable(I_Table, C_EffectName);
	LockStringTables(B_Save);
}

stock void PrecacheEffect() {
	static int I_Table = INVALID_STRING_TABLE;

	if (I_Table == INVALID_STRING_TABLE) {
		I_Table = FindStringTable("EffectDispatch");
	}

	bool B_Save = LockStringTables(false);
	AddToStringTable(I_Table, "ParticleEffect");
	LockStringTables(B_Save);
}

public void misc_RemoveFloat(float DXDX) {  }
